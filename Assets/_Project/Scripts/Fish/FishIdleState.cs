using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishIdleState : BaseState<FishController>
{
    public override void Enter()
    {
        base.Enter();
        owner.BotMover_.EnabedNavMesh(false);
        owner.BotMover_.enabled = false;
        owner.canvas.gameObject.SetActive(false);
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }
    public override void Exit()
    {
        base.Exit();
        owner.BotMover_.enabled = true;
        owner.BotMover_.EnabedNavMesh(true);
    }
}
