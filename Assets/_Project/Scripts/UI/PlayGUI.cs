using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayGUI : MonoBehaviour
{
    [SerializeField] ControlPositionJoyStick joystickController;

    [SerializeField] List<Joystick> joysticks;
        
    private void Awake()
    {
        foreach(var item in joysticks)
        {
            item.gameObject.SetActive(false);
        }
        SetJoyStick(JoyStickType.Fixed);
    }
    public void SetJoyStick(JoyStickType type)
    {
        joystickController.SetMainJoyStick(joysticks[(int)type]);
    }
    public void SetFixed()
    {
        Debug.Log("Set fix");
        SetJoyStick(JoyStickType.Fixed);
        //DataManager.Ins.IsFixedStick = true;
    }
    public void SetDynamic()
    {
        Debug.Log("Set Dynamic");
        SetJoyStick(JoyStickType.Dynamic);
        //DataManager.Ins.IsFixedStick = false;
    }

}

public enum JoyStickType
{
    Fixed,
    Dynamic
}
