using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThinkingIcon : MonoBehaviour
{
    [SerializeField] ListUpgrade listIcon;

    [SerializeField] Image icon;
    // Start is called before the first frame update
    void Start()
    {
        icon.sprite = listIcon.upgradeData[1].icon;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
