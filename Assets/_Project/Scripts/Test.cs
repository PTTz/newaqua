﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UCExtension.GUI;

public class Test : MonoBehaviour
{
    [SerializeField] GameObject lineRender;
    void LevelUp(int i)
    {
        //Debug.Log("sddsd");
        GUIController.Ins.ShowGUI("LevelUpGUI");
        //lineRender.SetActive(true);
    }

    void Test2(int i)
    {
        Debug.Log("test2");
    }
    // Start is called before the first frame update
    void Start()
    {
        DataManager.Ins.OnWorldLevelChange += LevelUp;
        DataManager.Ins.OnWorldLevelChange += Test2;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.O))
        {
            GUIController.Ins.ShowGUI("UpgradeGUI");
        }
        //test level up bằng nút bấm
        if (Input.GetKeyDown(KeyCode.L))
        {
            GUIController.Ins.ShowGUI("LevelUpGUI");
            DataManager.Ins.WorldLevel += 1;
        }
    }
}
