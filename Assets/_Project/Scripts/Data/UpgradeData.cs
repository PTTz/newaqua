using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="UpgradeInfor",menuName ="Create Upgrade")]
public class UpgradeData : ScriptableObject
{
    public string upgradeLevel;
    public string upgradeName;
    public Sprite icon;
    public List<LevelCost> costs;
}

[Serializable]
public class LevelCost
{
    public int MoneyCost;
    public int AdsCost;
    public float Value;
}