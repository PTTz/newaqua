using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CashierZone : MonoBehaviour
{
    public UnityAction<Collider> OnEnter;
    public UnityAction<Collider> OnExit;
    private void OnTriggerEnter(Collider other)
    {
        OnEnter?.Invoke(other);
    }
    private void OnTriggerStay(Collider other)
    {
        OnEnter?.Invoke(other);
    }
    private void OnTriggerExit(Collider other)
    {
        OnExit?.Invoke(other);
    }
}
