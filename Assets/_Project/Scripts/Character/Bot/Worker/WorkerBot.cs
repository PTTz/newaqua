using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorkerBot : Character
{
    public StateMachine<WorkerBot> stateMachine;
    public bool isSwiming;
    public bool isNearTarget;
    public float walkAnimSpeed;
    public float normalSwimSpeed;
    public float slowSwimSpeed;
    public FishController fishTargeted;
    public FishTank tankTargeted;
    public List<FishController> carringFishList;
    public int maxBaloSize;
    public FishColCatch fishColCatch;
    public BotMover botMover;
    public FishController pickedFish;
    public Transform baloPos;
    public bool oneTimeWorker;
    public Transform Home;
    public float NearHomeValue;

    [SerializeField] bool isHiring = false;
    [SerializeField] float hireTime;
    [SerializeField] float hireTimeCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        stateMachine = new StateMachine<WorkerBot>(this);
        stateMachine.ChangeState<WorkerIdleState>();
        fishColCatch.OnColCatchFish += CatchedNewFish;
        fishColCatch.SetOwnerColCatch(this);

        maxBaloSize += DataManager.Ins.GetUpgradeLevel(3);
    }

    // Update is called once per frame
    void Update()
    {
        stateMachine.CurrentState.LogicUpdate();
        TestCall();
    }
    public void ChangeHireTime(int newHireTime)
    {
        hireTime = newHireTime;
    }
    public void AnimationCheck()
    {
        if (isSwiming)
        {
            if (carringFishList.Count < maxBaloSize)
            {
                fishColCatch.gameObject.SetActive(true);
            }
            if (isNearTarget)
            {
                animationController.Swim(slowSwimSpeed);
            }
            else
            {
                animationController.Swim(normalSwimSpeed);
            }
        }
        else
        {
            animationController.WalkingSpeed(walkAnimSpeed);
        }
    }
    private void FixedUpdate()
    {
        if(isHiring)
        {
            if(oneTimeWorker)
            {
                HireOneTime();
            }
            else
            {
                HireByTime();
            }
        }
    }
    void HireByTime()
    {
        maxBaloSize = 5;
        hireTimeCounter += Time.deltaTime;
        if (hireTimeCounter >= hireTime)
        {
            isHiring = false;
            stateMachine.ChangeState<WorkerIdleState>();
            hireTimeCounter = 0;
        }
    }
    public void HireOneTime()
    {

    }
    public void CatchedNewFish(FishController newFish, Character charCatchFish)
    {
        foreach (var fish in carringFishList)
        {
            if (fish == newFish) return;
        }
        if (carringFishList.Count >= maxBaloSize) return;
        if (charCatchFish != this) return;
        carringFishList.Add(newFish);
        GameController.Ins.fishAbleToCatchList.Remove(newFish);
        newFish.gameObject.transform.parent = baloPos;
        newFish.Jump(baloPos.position);
        newFish.transform.DOScale(0.01f, 0.6f);
    }
    public void StartHired()
    {
        isHiring = true;
        transform.parent = null;
        stateMachine.ChangeState<WorkerGoFishingState>();
    }
    public void ThrowFishToTank(Collider other)
    {
        var tank = other.GetComponent<FishTank>();
        FishType tankFishType = tank.defaultFish.Fish.FishType_;
        Transform tankCenter = tank.tankCenter;

        foreach (var fish in carringFishList.ToList())
        {
            if (fish.Fish.FishType_ == tankFishType)
            {
                fish.Jump(tankCenter.position, 0.5f, finish: () =>
                {
                    fish.ToTheTank();
                });
                fish.transform.DOScale(0.3f, 0.6f);
                fish.transform.parent = other.transform;
                carringFishList.Remove(fish);
                GameController.Ins.PushNewFishToTank(fish);
                tank.FishOnTankList.Add(fish);
            }
        }
    }

    public bool CheckDistant(Transform from, Transform to, float isSmallerThisFloat)
    {
        float distant = 0;
        distant = Vector3.Distance(from.position, to.position);
        if (distant > isSmallerThisFloat)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    void TestCall()
    {
        if(Input.GetKeyDown(KeyCode.H))
        {
            StartHired();
        }
    }
}
