﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UCExtension.GUI;
using UCExtension.Common;

public class LevelupGUI : BaseGUI
{
    [SerializeField] Button closeButton;
    [SerializeField] Text levelText;
    [SerializeField] Image fishIcon;
    [SerializeField] Text fishName;
    [SerializeField] Animator levelUpAnim;

    //[SerializeField] GameObject fish3D;
    int level;
    // Start is called before the first frame update
    void Start()
    {
        //fish3D = GameObject.FindGameObjectWithTag("UIFish");
        closeButton.onClick.AddListener(Close);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Close()
    {
        //fish3D.GetComponent<Fish3DShow>().NotShowing();
        base.Close();
    }

    private void OnEnable()
    {
        
        Debug.Log("Still ok");
        //fish3D.GetComponent<Fish3DShow>().OnShowing();
        //khi được enable sẽ chạy levelup
        levelUpAnim.SetTrigger("leveluppop");
        LevelUp();
        
    }
    public void LevelUp()
    {
        Debug.Log("runnign");
        level = DataManager.Ins.WorldLevel;
        Debug.Log(level);
        levelText.text = level.ToString();
        fishName.text = DataManager.Ins.GetFishName(level - 1);
        fishIcon.sprite = DataManager.Ins.GetFishImage(level-1);
        fishIcon.SetNativeSize();
    }
}
