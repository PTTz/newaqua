using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSetting : MonoBehaviour
{
    public bool buttonStatus;

    [SerializeField] Sprite imageOn;

    [SerializeField] Sprite imageOff;

    public int OnClickButton(bool status)
    {
        buttonStatus = status;
        if(buttonStatus==true)
        {
            this.GetComponent<Image>().sprite = imageOn;
            buttonStatus = true;
            return 1;
        }
        else
        {
            this.GetComponent<Image>().sprite = imageOff;
            buttonStatus = false;
            return 0;
        }
    }

}
