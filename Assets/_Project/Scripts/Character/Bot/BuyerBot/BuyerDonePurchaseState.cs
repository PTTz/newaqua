using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyerDonePurchaseState : BotBaseState
{
    public override void EnterState(BuyerBot buyer)
    {
        buyer.Canvas.gameObject.SetActive(true);
        buyer.HappyFace();
        buyer.TargetPos = GameController.Ins.DoorWay;
        buyer.GiveMoney(buyer.transform);
        buyer.RunWithFishInHead();
    }
    public override void UpdateState(BuyerBot buyer)
    {
        GoHome(buyer);
    }
    void GoHome(BuyerBot buyer)
    {
        buyer.botMover.SetDestination(buyer.TargetPos.position);
    }
    public override void OnTriggerEnter(BuyerBot buyer, Collider other)
    {
        if(other.tag.Equals(TagName.DoorWay))
        {
            buyer.Recyle();
        }
    }
    public override void ExitState(BuyerBot buyer)
    {

    }
}
