Shader "Custom/Outline Fill"
{
  Properties
  {
    [Enum(UnityEngine.Rendering.CompareFunction)] _ZTest ("ZTest", float) = 0
    _OutlineColor ("Outline Color", Color) = (1,1,1,1)
    _OutlineWidth ("Outline Width", Range(0, 10)) = 2
  }
  SubShader
  {
    Tags
    { 
      "DisableBatching" = "true"
      "QUEUE" = "Transparent+110"
      "RenderType" = "Transparent"
    }
    Pass // ind: 1, name: Fill
    {
      Name "Fill"
      Tags
      { 
        "DisableBatching" = "true"
        "QUEUE" = "Transparent+110"
        "RenderType" = "Transparent"
      }
      ZWrite Off
      Cull Off
      Stencil
      { 
        Ref 1
        ReadMask 255
        WriteMask 255
        Pass Keep
        Fail Keep
        ZFail Keep
        PassFront Keep
        FailFront Keep
        ZFailFront Keep
        PassBack Keep
        FailBack Keep
        ZFailBack Keep
      } 
      Blend SrcAlpha OneMinusSrcAlpha
      ColorMask RGB
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      #define conv_mxt4x4_0(mat4x4) float4(mat4x4[0].x,mat4x4[1].x,mat4x4[2].x,mat4x4[3].x)
      #define conv_mxt4x4_1(mat4x4) float4(mat4x4[0].y,mat4x4[1].y,mat4x4[2].y,mat4x4[3].y)
      #define conv_mxt4x4_2(mat4x4) float4(mat4x4[0].z,mat4x4[1].z,mat4x4[2].z,mat4x4[3].z)
      #define conv_mxt4x4_3(mat4x4) float4(mat4x4[0].w,mat4x4[1].w,mat4x4[2].w,mat4x4[3].w)
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_WorldToObject;
      //uniform float4x4 UNITY_MATRIX_P;
      //uniform float4x4 unity_MatrixV;
      //uniform float4x4 unity_MatrixInvV;
      uniform float4 _OutlineColor;
      uniform float _OutlineWidth;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float3 normal :NORMAL0;
          float3 texcoord3 :TEXCOORD3;
      };
      
      struct OUT_Data_Vert
      {
          float4 color :COLOR0;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float4 color :COLOR0;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      float3 u_xlat2;
      float u_xlat9;
      int u_xlatb9;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          u_xlat0.xyz = (conv_mxt4x4_1(unity_WorldToObject).xyz * conv_mxt4x4_0(unity_MatrixInvV).yyy);
          u_xlat0.xyz = ((conv_mxt4x4_0(unity_WorldToObject).xyz * conv_mxt4x4_0(unity_MatrixInvV).xxx) + u_xlat0.xyz);
          u_xlat0.xyz = ((conv_mxt4x4_2(unity_WorldToObject).xyz * conv_mxt4x4_0(unity_MatrixInvV).zzz) + u_xlat0.xyz);
          u_xlat0.xyz = ((conv_mxt4x4_3(unity_WorldToObject).xyz * conv_mxt4x4_0(unity_MatrixInvV).www) + u_xlat0.xyz);
          u_xlat9 = dot(in_v.texcoord3.xyz, in_v.texcoord3.xyz);
          #ifdef UNITY_ADRENO_ES3
          u_xlatb9 = (u_xlat9!=0);
          #else
          u_xlatb9 = (u_xlat9!=0);
          #endif
          u_xlat1.xyz = (int(u_xlatb9))?(in_v.texcoord3.xyz):(in_v.normal.xyz);
          u_xlat0.x = dot(u_xlat0.xyz, u_xlat1.xyz);
          u_xlat2.xyz = mul(unity_MatrixInvV, unity_WorldToObject[0]);
          u_xlat0.y = dot(u_xlat2.xyz, u_xlat1.xyz);
          u_xlat2.xyz = (conv_mxt4x4_1(unity_WorldToObject).xyz * conv_mxt4x4_2(unity_MatrixInvV).yyy);
          u_xlat2.xyz = ((conv_mxt4x4_0(unity_WorldToObject).xyz * conv_mxt4x4_2(unity_MatrixInvV).xxx) + u_xlat2.xyz);
          u_xlat2.xyz = ((conv_mxt4x4_2(unity_WorldToObject).xyz * conv_mxt4x4_2(unity_MatrixInvV).zzz) + u_xlat2.xyz);
          u_xlat2.xyz = ((conv_mxt4x4_3(unity_WorldToObject).xyz * conv_mxt4x4_2(unity_MatrixInvV).www) + u_xlat2.xyz);
          u_xlat0.z = dot(u_xlat2.xyz, u_xlat1.xyz);
          u_xlat0.xyz = normalize(u_xlat0.xyz);
          u_xlat1 = mul(unity_ObjectToWorld, float4(in_v.vertex.xyz,1.0));
          u_xlat2.xyz = (u_xlat1.yyy * conv_mxt4x4_1(unity_MatrixV).xyz);
          u_xlat2.xyz = ((conv_mxt4x4_0(unity_MatrixV).xyz * u_xlat1.xxx) + u_xlat2.xyz);
          u_xlat1.xyz = ((conv_mxt4x4_2(unity_MatrixV).xyz * u_xlat1.zzz) + u_xlat2.xyz);
          u_xlat1.xyz = ((conv_mxt4x4_3(unity_MatrixV).xyz * u_xlat1.www) + u_xlat1.xyz);
          u_xlat0.xyz = (u_xlat0.xyz * (-u_xlat1.zzz));
          u_xlat0.xyz = (u_xlat0.xyz * float3(_OutlineWidth, _OutlineWidth, _OutlineWidth));
          u_xlat0.xyz = ((u_xlat0.xyz * float3(0.00100000005, 0.00100000005, 0.00100000005)) + u_xlat1.xyz);
          out_v.vertex = mul(UNITY_MATRIX_P, float4(u_xlat0.xyz,1.0));
          out_v.color = _OutlineColor;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          out_f.color = in_f.color;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}
