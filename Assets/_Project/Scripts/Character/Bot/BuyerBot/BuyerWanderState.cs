
using System.Collections.Generic;
using UnityEngine;
using UCExtension.Common;
using UCExtension;

public class BuyerWanderState : BotBaseState
{
    public override void EnterState(BuyerBot buyer)
    {
        TargetTank(buyer);
    }
    void TargetTank(BuyerBot buyer)
    {
        var randomTank = GameController.Ins.GetRandomTankHaveFish();
        if (randomTank)
        {
            buyer.TankTargeting = randomTank;
            buyer.TargetPos = randomTank.transform;
        }
    }
    public override void UpdateState(BuyerBot buyer)
    {
        if (buyer.TargetPos)
        {
            buyer.botMover.SetDestination(buyer.TargetPos.position);
            buyer.Walk(2);
        }
        else
        {
            TargetTank(buyer);
        }
    }
    public override void OnTriggerEnter(BuyerBot buyer, Collider other)
    {
        if (other.tag.Equals(TagName.FishTankStayZone))
        {
            if(buyer.TankTargeting == other.GetComponent<FishTank>())
            {
                buyer.SwitchState(buyer.choosingFishState);
            }
        }
    }
    public override void ExitState(BuyerBot buyer)
    {

    }
}
