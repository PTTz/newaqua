using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UCExtension.Common;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.Events;

public class FishController : MovingObject
{
    public FishData Fish;
    public FishAnimationController FishAnimationController_;
    public Animator fishAnim;
    public Transform RealBody;
    public Vector3 TargetPos;
    public float LimitLeft = 24f;
    public float LimitRight = 105f;
    public float LimitTop = 12f;
    public float LimitBottom = -58f;
    public Transform hunterTranform;
    public StateMachine<FishController> stateMachine;
    public BotMover BotMover_;
    public int HPFish { get; private set; }

    [SerializeField] Image HpBar;

    public Canvas canvas;

    [SerializeField] TextMeshPro levelText;

    [SerializeField] GameObject levelBox;

    [SerializeField] GameObject fishCanvas;
    bool canCatch = true;

    public UnityAction<FishController, Character> OnCatched;

    private void Awake()
    {
        HPFish = Fish.FishHP;
    }
    private void Start()
    {
        stateMachine = new StateMachine<FishController>(this);
        stateMachine.ChangeState<FishIdleOnSeaState>();
        Warp();
        if(FishAnimationController_)
        {
            FishAnimationController_.SetSwimSpeed(Fish.NormalSpeed);
        }
        if(fishAnim)
        {
            fishAnim.Play("BlueAngleSwim");
        }
    }
    private void Update()
    {
        stateMachine.CurrentState.LogicUpdate();
    }
    public void HideLevel()
    {
        levelBox.SetActive(false);
    }
    public void Warp()
    {
        BotMover_.Warp();
    }
    public void ShowInfo(int level)
    {
        fishCanvas.SetActive(true);
        levelText.text = Fish.FishLevel.ToString();
    }
    public void ToTheTank()
    {
        stateMachine.ChangeState<FishIdleOnTankState>();
        Warp();
    }

    public void HideInfo()
    {
        fishCanvas.SetActive(false);
    }

    public void Attacked(Character hunter, int atk)
    {
        hunterTranform = hunter.transform;
        canvas.gameObject.SetActive(true);
        if (HPFish <= 0 && canCatch)
        {
            canCatch = false;
            Catched(hunter);
        }
        else
        {
            if(!stateMachine.IsCurrentState<FishIdleState>())
            {
                stateMachine.ChangeState<FishEscapingState>();
            }
            HPFish -= atk;
            HpBar.fillAmount = (float)HPFish / Fish.FishHP;
        }
    }
    public void Escaped()
    {
        if(!stateMachine.IsCurrentState<FishIdleState>())
        {
            stateMachine.ChangeState<FishIdleOnSeaState>();
        }
    }
    public void Catched(Character charCatchedFish)
    {
        stateMachine.ChangeState<FishIdleState>();
        GameController.Ins.fishAbleToCatchList.Remove(this);
        OnCatched?.Invoke(this, charCatchedFish);
    }
    public void OnTank()
    {
        stateMachine.ChangeState<FishIdleOnTankState>();
        canvas.gameObject.SetActive(false);
    }
    public bool IsNearTarget(Vector3 from, Vector3 to, float isSmallerThisFloat)
    {
        float distant = 0;
        distant = Vector3.Distance(from, to);
        if (distant > isSmallerThisFloat)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public override void Recyle()
    {
        base.Recyle();
        Reset();
    }
    public void Reset()
    {
        HpBar.fillAmount = 1;
        HPFish = Fish.FishHP;
        canvas.gameObject.SetActive(true);
        canCatch = true;
    }
}
