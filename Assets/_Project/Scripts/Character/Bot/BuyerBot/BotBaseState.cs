using UnityEditor;
using UnityEngine;

public abstract class BotBaseState 
{
    public abstract void EnterState(BuyerBot buyer);
    public abstract void UpdateState(BuyerBot buyer);
    public abstract void OnTriggerEnter(BuyerBot buyer, Collider other);
    public abstract void ExitState(BuyerBot buyer);
}
