using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UCExtension.Common;
using UnityEngine.UI;
using System;
using UCExtension;
using DG.Tweening;
using API.Ads;

public class GameController : Singleton<GameController>
{
    public bool IsDoneLoad;
    public Player MainPlayer;
    public Transform BotCustomerPos;
    public CashierTable CashierTable;
    public WorkerBot MainWorkerBot;
    public Transform DoorWay;
    public MoneyZone MoneyZone;
    public Camera Camera_;
    public RecylableObject CustomerBot;
    public List<DefaultFishOnScene> DefaultFish;
    public List<BuyerBot> customerBots;
    [SerializeField] GameObject[] listTutorial;
    [SerializeField] TutorialLine lineRender;

    public List<FishTank> FishTanks;

    public List<FishController> FishOnTankList;

    public List<FishController> fishControllers;

    public List<FishController> fishAbleToCatchList;

    public Transform PoolOfFishTransform;

    float delayShowFull = 90;
    float elapsed = 0f;

    private void Start()
    {
        IsDoneLoad = false;
        SpawnFish();
        DataManager.Ins.OnWorldLevelChange += OnChangeWorldLevel;
        ResetAbleFish(DataManager.Ins.WorldLevel);
        DataManager.Ins.OnShowfulled += OnShowFulled;
        //TutorialUnlock(DataManager.Ins.UnlockedTutorial+1);
        if(DataManager.Ins.UnlockedTutorial <= listTutorial.Length)
            lineRender.endPos = listTutorial[DataManager.Ins.UnlockedTutorial - 1].transform.position;
        else
            lineRender.gameObject.SetActive(false);
    }
    void OnShowFulled(int coinClaimed)
    {
        delayShowFull = 90 - DataManager.Ins.ShowFulled * 5;
        if (DataManager.Ins.ShowFulled >= 6) delayShowFull = 60;
    }
    void ShowFullAds()
    {
        if (DataManager.Ins.CoinClaimed < 500) return;
        elapsed += Time.deltaTime;
        if (elapsed >= delayShowFull)
        {
            elapsed = 0;
            if (AdManager.Ins)
            {
                AdManager.Ins.ShowFull("SHOWFULL", () =>
                {
                    DataManager.Ins.ShowFulled++;
                });
            }
        }
    }
    private void Update()
    {
        ShowFullAds();
    }
    public void WorkerSpeedChange(float speed)
    {
        MainWorkerBot.GetComponent<BotMover>().SpeedChange(speed);
    }
    public void WorkerIncreateHireTime(int newTime)
    {
        MainWorkerBot.ChangeHireTime(newTime);
    }

    public void TutorialUnlock(int index)
    {
        if (DataManager.Ins.UnlockedTutorial == index - 1 || DataManager.Ins.UnlockedTutorial == index)
        {
            DataManager.Ins.UnlockedTutorial = index;
            if (DataManager.Ins.UnlockedTutorial <= listTutorial.Length)
            {
                if (DataManager.Ins.UnlockedTutorial == 2)
                {
                    TurnOnOffLineRender();
                }
                //listTutorial[index - 1].SetActive(true);
                lineRender.gameObject.SetActive(true);
                lineRender.endPos = listTutorial[index - 1].transform.position;
            }
            else
            {
                lineRender.gameObject.SetActive(false);
            }
        }
    }
    public void TurnOnOffLineRender()
    {
        lineRender.gameObject.SetActive(!lineRender.gameObject.activeSelf);
    }
    public FishTank GetRandomTankHaveFish()
    {
        List<FishTank> tankHaveFish = new List<FishTank>();
        foreach (var tank in FishTanks)
        {
            if (tank.FishOnTankList.Count > 0)
            {
                tankHaveFish.Add(tank);
            }
        }
        if (tankHaveFish.Count > 0)
        {
            return tankHaveFish.RandomElement();
        }
        return null;
    }
    void SpawnFish()
    {
        foreach (DefaultFishOnScene fish in DefaultFish)
        {
            IsDoneLoad = false;
            int index = 0;
            foreach (Level level in fish.level)
            {
                index++;
                if (index <= DataManager.Ins.WorldLevel)
                {
                    SpawnFishModun(level, fish);
                }
            }
        }
        IsDoneLoad = true;
        OnChangeWorldLevel(DataManager.Ins.WorldLevel);
    }
    public void SpawnMoreFish(int levelNum)
    {
        foreach (DefaultFishOnScene fish in DefaultFish)
        {
            IsDoneLoad = false;
            int index = 0;
            foreach (Level level in fish.level)
            {
                index++;
                if (index == levelNum)
                {
                    SpawnFishModun(level, fish);
                }
            }
        }
        IsDoneLoad = true;
    }
    void SpawnFishModun(Level level, DefaultFishOnScene fish)
    {
        if (level.spawnMoreFish)
        {
            for (int i = 0; i < level.numberOfFishToSpawn; i++)
            {
                Vector3 pos = Vector3.zero;
                float x = UnityEngine.Random.Range(20, 100);
                float z = UnityEngine.Random.Range(-60, -15);
                pos.x = x;
                pos.z = z;
                var newFish = PoolObjects.Ins.SpawnObject(fish.FishObj, pos);
                var newFishController = newFish.GetComponent<FishController>();
                fishControllers.Add(newFishController);
            }
        }
    }
    public void PushNewFishToTank(FishController newFish)
    {
        FishOnTankList.Add(newFish);
        StartCoroutine(IESpawnCustomerBot(newFish));
    }
    IEnumerator IESpawnCustomerBot(FishController newFish)
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(1, 10f));
        SpawnNewCustomerBot(newFish);
    }
    void SpawnNewCustomerBot(FishController newFish)
    {
        if (customerBots.Count >= DataManager.Ins.maxCustommerSceneHave()) return;
        var newCustomer = PoolObjects.Ins.SpawnObject(CustomerBot, BotCustomerPos.position);
        var newBuyer = newCustomer.GetComponent<BuyerBot>();
        customerBots.Add(newBuyer);
    }
    public void OnChangeWorldLevel(int worldLevel)
    {
        SpawnMoreFish(worldLevel);
        ResetAbleFish(worldLevel);
    }
    void ResetAbleFish(int worldLevel)
    {
        fishAbleToCatchList.Clear();
        foreach (var fish in fishControllers)
        {
            if (fish.Fish.FishLevel <= worldLevel)
            {
                fishAbleToCatchList.Add(fish);
            }
        }
    }
}
[Serializable]
public class DefaultFishOnScene
{
    public List<Level> level;
    public RecylableObject FishObj;
}
[Serializable]
public class Level
{
    public bool spawnMoreFish;
    public int numberOfFishToSpawn;
}
