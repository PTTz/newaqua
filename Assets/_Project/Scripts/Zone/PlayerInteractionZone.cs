using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractionZone : MonoBehaviour
{
    const float DELAY_INTERACTION_TIME = 0.05f;

    [SerializeField] protected Transform workerSlot;

    public Transform WorkerSlot => workerSlot;

    DelayCaller caller;

    Player stayingPlayer;

    public virtual void Awake()
    {
        caller = new DelayCaller(DELAY_INTERACTION_TIME);
    }

    protected virtual void Update()
    {
        if (stayingPlayer)
        {
            if (caller.Call())
            {
                OnPlayerStay(stayingPlayer);
            }
        }
    }
    public virtual void OnPlayerEnter(Player player)
    {
        stayingPlayer = player;
    }

    public virtual void OnPlayerStay(Player player)
    {

    }

    public virtual void OnPlayerExit(Player player)
    {
        stayingPlayer = null;
    }
}
