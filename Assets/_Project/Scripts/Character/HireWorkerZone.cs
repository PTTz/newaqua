using System.Collections;
using System.Collections.Generic;
using UCExtension.GUI;
using UnityEngine;

public class HireWorkerZone : PlayerInteractionZone
{
    [SerializeField] WorkerZone workerZone;

    [SerializeField] RushWorkerZone rushWorkerZone;

    WorkerGUI WorkerGUI;

    WorkerGUI WorkerRushGUI;

    bool isRush;
    private void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if (workerZone != null)
            {
                WorkerGUI = GUIController.Ins.GetGUI("HireWorkerGUI").gameObject.GetComponent<WorkerGUI>();
                WorkerGUI.SetWorker(workerZone.mainWorkerBot);
            }
            if (rushWorkerZone != null)
            {
                WorkerRushGUI = GUIController.Ins.GetGUI("WorkerRushGUI").gameObject.GetComponent<WorkerGUI>();
                WorkerRushGUI.SetRushWorker(rushWorkerZone.mainWorkerBot);
                isRush = true;
            }
        }
    }

    public override void OnPlayerEnter(Player player)
    {
        base.OnPlayerEnter(player);
        if (!isRush)
        {
            WorkerGUI.gameObject.SetActive(true);
            GUIController.Ins.ShowGUI("HireWorkerGUI");
        }
        else
        {
            WorkerRushGUI.gameObject.SetActive(true);
            GUIController.Ins.ShowGUI("WorkerRushGUI");
        }

    }

    public override void OnPlayerExit(Player player)
    {
        base.OnPlayerExit(player);
        if (!isRush)
        {
            WorkerGUI.gameObject.SetActive(false);
            GUIController.Ins.HideGUI("HireWorkerGUI");
        }
        else
        {
            WorkerRushGUI.gameObject.SetActive(false);
            GUIController.Ins.HideGUI("WorkerRushGUI");
        }
    }
}
