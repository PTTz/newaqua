
using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;

public class BuyerChoosingFishState : BotBaseState
{
    float delay = 0;
    public override void EnterState(BuyerBot buyer)
    {
        delay = UnityEngine.Random.Range(2, 10);
        buyer.INeedThisFish(buyer.TankTargeting.defaultFish.Fish);
        float offsetTime = UnityEngine.Random.Range(0, 5);
        buyer.Walk(0.3f);
    }
    void PickFish(BuyerBot buyer)
    {
        var pickedFish = buyer.TankTargeting.OnBuyerPickFish(buyer);
        if(pickedFish == null)
        {
            buyer.WhereIsMyFish();
            pickedFish = buyer.TankTargeting.OnBuyerPickFish(buyer);
            return;
        }
        pickedFish.Jump(buyer.FishPos.position, finish: () =>
        {
            pickedFish.transform.parent = buyer.FishPos;
            buyer.carringFish = pickedFish;
            pickedFish.transform.localPosition = Vector3.zero;
            buyer.SwitchState(buyer.gotoPurchaseState);
            pickedFish.stateMachine.ChangeState<FishIdleState>();
            GameController.Ins.FishOnTankList.Remove(pickedFish);
        });
    }
    public override void UpdateState(BuyerBot buyer)
    {
        if (delay <= 0)
        {
            delay = 5;
            if (buyer.carringFish) return;
            PickFish(buyer);
        }
        else
        {
            delay -= Time.deltaTime;
        }
    }
    public override void OnTriggerEnter(BuyerBot buyer, Collider other)
    {
        if (other.tag.Equals(TagName.CusTommerStopZone))
        {
            FishTank tank = other.GetComponent<StopZone>().FishTank;
            if(tank == buyer.TankTargeting)
            {
                buyer.Walk(0);
            }
        }
    }
    public override void ExitState(BuyerBot buyer)
    {
        buyer.Canvas.gameObject.SetActive(false);
        buyer.miniTank.SetActive(true);
    }
}
