using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishIdleOnSeaState : BaseState<FishController>
{
    float posX = 0;
    float posZ = 0;
    public override void Enter()
    {
        base.Enter();
        owner.BotMover_.SpeedChange(owner.Fish.NormalSpeed);
        TargetNewPos();
    }
    void PosControll()
    {
        float limitLeft = owner.LimitLeft;
        float limitRight = owner.LimitRight;
        float limitTop = owner.LimitTop;
        float limitBottom = owner.LimitBottom;
        float x = owner.TargetPos.x;
        float z = owner.TargetPos.z;
        if (x < limitLeft || x > limitRight)
        {
            TargetNewPos();
        }
        if (z < limitBottom || z > limitTop)
        {
            TargetNewPos();
        }
    }
    void TargetNewPos()
    {
        float newX = UnityEngine.Random.Range(-20, 20);
        float newZ = UnityEngine.Random.Range(-20, 20);
        Vector3 offset = new Vector3(newX, 0, newZ);
        owner.TargetPos = owner.transform.position + offset;
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
        float posX = owner.transform.position.x;
        float posZ = owner.transform.position.z;
        PosControll();
        owner.BotMover_.SetDestination(owner.TargetPos);
        if (owner.IsNearTarget(owner.transform.position, owner.TargetPos, 2f))
        {
            TargetNewPos();
        }
    }
    public override void Exit()
    {
        base.Exit();
    }
}
