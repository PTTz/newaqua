using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishTankRoot : MonoBehaviour
{
    public FishData FishData_;

    public int FishTankLevel;

    public int MaxTankCapacity;

    [SerializeField] BuyTankZone buyTankZone;

    public FishTank fishTank;

    private void Start()
    {
        DataManager.Ins.OnWorldLevelChange += OnWorldLevelChange;
        OnWorldLevelChange(DataManager.Ins.WorldLevel);
        SetUp();
    }
    void SetUp()
    {
        buyTankZone.FishData_ = FishData_;
        fishTank.MaxTankCapacity = MaxTankCapacity;
        buyTankZone.TankLevel = FishTankLevel;
    }
    void OnWorldLevelChange(int worldLevel)
    {

        if (FishTankLevel <= worldLevel + 1)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
