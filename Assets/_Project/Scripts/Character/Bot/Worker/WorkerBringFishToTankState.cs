
using UnityEngine;

public class WorkerBringFishToTankState : BaseState<WorkerBot>
{
    public override void Enter()
    {
        base.Enter();
        if(owner.carringFishList.Count>0)
        {
            owner.pickedFish = owner.carringFishList[0];
            owner.fishColCatch.gameObject.SetActive(false);
            TargetNewFishTank();
        }
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
        owner.AnimationCheck();
        if (!owner.tankTargeted)
        {
            TargetNewFishTank();
        }
        else
        {
            owner.botMover.SetDestination(owner.tankTargeted.gameObject.transform.position);
        }
        if (owner.CheckDistant(owner.transform, owner.tankTargeted.transform, 4))
        {
            OnNearTarget();
        }
        if(owner.carringFishList.Count == 0)
        {
            if(owner.oneTimeWorker)
            {
                owner.stateMachine.ChangeState<WorkerIdleState>();
            }
            else
            {
                owner.stateMachine.ChangeState<WorkerGoFishingState>();
            }
        }
    }

    void OnNearTarget()
    {
        owner.ThrowFishToTank(owner.tankTargeted.GetComponent<Collider>());
        if (owner.carringFishList.Count == 0) return;
        owner.pickedFish = owner.carringFishList[0];
        TargetNewFishTank();
    }
    public FishController PickOtherFishType(FishController oldFish)
    {
        foreach (var fish in owner.carringFishList)
        {
            if (fish.Fish.FishType_ != oldFish.Fish.FishType_)
            {
                return fish;
            }
        }
        return null;
    }
    void TargetNewFishTank()
    {
        foreach (var tank in GameController.Ins.FishTanks)
        {
            if (tank.defaultFish.Fish.FishType_ == owner.pickedFish.Fish.FishType_)
            {
                owner.tankTargeted = tank;
                if(tank.FishOnTankList.Count >= tank.MaxTankCapacity)
                {
                    owner.pickedFish = PickOtherFishType(owner.pickedFish);
                }
                return;
            }
        }
    }
    public override void Exit()
    {
        base.Exit();
    }
}
