using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FishColCatch : MonoBehaviour
{
    public int ATK;
    [SerializeField] Character ownerOfColCatch;
    [SerializeField] bool isPlayer;
    [SerializeField] FishController TargetingFish;
    [SerializeField] List<FishController> FishStandingOnColList;

    public UnityAction<FishController, Character> OnColCatchFish;

    public void SetOwnerColCatch(Character owner)
    {
        ownerOfColCatch = owner;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals(TagName.HookAble))
        {
            if (!IsDuplicateFish(other))
            {
                FishController newFish = other.GetComponent<FishController>();
                FishStandingOnColList.Add(newFish);
            }
            if (TargetingFish)
            {
                TargetingFish.OnCatched += OnCatchedFish;
                Catching();
            }
            else
            {
                TargetOnlyoneFish();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals(TagName.HookAble))
        {
            TargetingFish = null;
            FishController oldFish = other.GetComponent<FishController>();
            oldFish.Escaped();
            oldFish.Reset();
            //an thanh mau va level
            oldFish.HideInfo();
            FishStandingOnColList.Remove(oldFish);
        }
    }
    bool IsDuplicateFish(Collider fishCol)
    {
        foreach (var fish in FishStandingOnColList)
        {
            if (fish == fishCol.GetComponent<FishController>()) return true;
        }
        return false;
    }
    void TargetOnlyoneFish()
    {
        if (TargetingFish != null)
        {
            return;
        }

        foreach (var item in FishStandingOnColList)
        {
            if (item.Fish.FishLevel <= DataManager.Ins.WorldLevel)
            {
                TargetingFish = item;
                //show thanh mau nhung an level
                item.ShowInfo(item.Fish.FishLevel);
                item.HideLevel();
                return;
            }
            else
            {
                //show ca thanh mau lan level
                item.ShowInfo(item.Fish.FishLevel);
            }
        }
    }
    void Catching()
    {
        if (TargetingFish == null)
        {
            return;
        }
        TargetingFish.Attacked(ownerOfColCatch, ATK);
    }
    void OnCatchedFish(FishController fish, Character charCatchedFish)
    {
        OnColCatchFish?.Invoke(fish, charCatchedFish);
        TargetingFish = null;
        FishStandingOnColList.Remove(fish);
    }
}
