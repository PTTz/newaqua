using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UCExtension;
using UnityEngine;

public abstract class StackCollector : MonoBehaviour
{
    [SerializeField] protected Transform maxObject;

    [SerializeField] protected bool canVibrate;


    public Action OnCollectObject;

    public Action OnEmptyObject;

    Dictionary<int, int> collectedIDs = new Dictionary<int, int>();

    public Dictionary<int, int> CollectedIDs
    {
        get
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            foreach (var item in collectedIDs)
            {
                if (item.Value > 0)
                {
                    result.Add(item.Key, 0);
                }
            }
            return result;
        }
    }

    const float TIME_COLLECT = 0.3f;

    public abstract bool CanCollect { get; }



    protected abstract Vector3 GetPushPosition();

    protected abstract Vector3 GetMaxPosition();

    protected abstract void ReorderObjects();

    private void Start()
    {
        if (maxObject)
        {
            maxObject.gameObject.SetActive(false);
        }
    }

    //public virtual void Collect(Product objectToCollect)
    //{
    //    OnCollectObject?.Invoke();
    //    objectToCollect.transform.SetParent(transform);
    //    var collectPosition = GetPushPosition();
    //    collectedObjects.Add(objectToCollect);
    //    objectToCollect.LocalJump(collectPosition, TIME_COLLECT);
    //    if (maxObject)
    //    {
    //        maxObject.gameObject.SetActive(!CanCollect);
    //        maxObject.transform.localPosition = GetMaxPosition();
    //    }
    //    AddToCollectedID(objectToCollect.CurrentData.ID);
    //    Vibrate();
    //}

    //public Product Pop(int id)
    //{
    //    var index = collectedObjects.FindLastIndex(x => x.CurrentData.ID == id);
    //    if (index >= 0)
    //    {
    //        var returnObject = collectedObjects[index];
    //        collectedObjects.RemoveAt(index);
    //        if (maxObject)
    //        {
    //            maxObject.gameObject.SetActive(false);
    //        }
    //        CheckCollectedObjectSize();
    //        ReorderObjects();
    //        Vibrate();
    //        RemoveFromCollectedID(id);
    //        return returnObject;
    //    }
    //    return null;
    //}

    //public List<Product> PopAll()
    //{
    //    var result = collectedObjects.Clone();
    //    collectedObjects.Clear();
    //    return result;
    //}

    //public Product PopLast()
    //{
    //    var result = collectedObjects.PopLastElement();
    //    if (maxObject)
    //    {
    //        maxObject.gameObject.SetActive(false);
    //    }
    //    if (result)
    //    {
    //        RemoveFromCollectedID(result.CurrentData.ID);
    //    }
    //    CheckCollectedObjectSize();
    //    Vibrate();
    //    return result;
    //}

    void Vibrate()
    {
        if (canVibrate)
        {
            //GameManager.Ins.Vibrate();
        }
    }

    void AddToCollectedID(int ID)
    {
        if (collectedIDs.ContainsKey(ID))
        {
            collectedIDs[ID]++;
        }
        else
        {
            collectedIDs[ID] = 1;
        }
    }

    void RemoveFromCollectedID(int ID)
    {
        if (collectedIDs.ContainsKey(ID))
        {
            collectedIDs[ID]--;
        }
    }

    //void CheckCollectedObjectSize()
    //{
    //    if (collectedObjects.Count == 0)
    //        OnEmptyObject?.Invoke();
    //}
}
