using System.Collections.Generic;
using UCExtension.Common;
using UnityEngine;
using UnityEngine.Events;

public class DataManager : Singleton<DataManager>
{
    [SerializeField] int defaultCoin;
    [SerializeField] int defaultUsedCoin;
    [SerializeField] int defaultLevel;
    [SerializeField] int defaultTutorial;
    [SerializeField] ListUpgrade listUpgrades;
    [SerializeField] List<int> listClaimUpgrade;
    [SerializeField] ListFish listFishes;
    [SerializeField] PlayerStats playerStats;
    [SerializeField] CustommerData CustommerData;
    public List<float> showFullTimes;
    public UnityAction OnCoinChange;
    public UnityAction<int> OnShowfulled;
    public UnityAction OnCoinUsedChange;
    public UnityAction OnClaimChange;
    public UnityAction<int> OnWorldLevelChange;

    public int Coin
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.COIN, defaultCoin);
        }
        set
        {
            if(value > Coin)
            {
                CoinClaimed += value - Coin;
            }
            PlayerPrefs.SetInt(PlayerPrefKeys.COIN, value);
            OnCoinChange?.Invoke();
        }
    }
    public int CoinClaimed
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.COIN_CLAIMED, 0);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.COIN_CLAIMED, value);
        }
    }
    public int ShowFulled
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.SHOW_FULLED, 0);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.SHOW_FULLED, value);
            OnShowfulled?.Invoke(ShowFulled);
        }
    }
    public int UsedCoin
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.USED_COIN, 0);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.USED_COIN, value);
            OnCoinUsedChange?.Invoke();
        }
    }

    public int WorldLevel
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.WORLD_LEVEL, defaultLevel);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.WORLD_LEVEL, value);
            OnWorldLevelChange?.Invoke(value);
        }
    }

    public int UnlockedTutorial
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.UNLOCKED_TUTORIAL, defaultTutorial);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.UNLOCKED_TUTORIAL, value);
        }
    }

    public int GetUpgradeLevel(int upgradeIndex)
    {
        return PlayerPrefs.GetInt(listUpgrades.upgradeData[upgradeIndex].upgradeLevel);
    }

    public void SetUpgradeLevel(int upgradeIndex, int level)
    {
        PlayerPrefs.SetInt(listUpgrades.upgradeData[upgradeIndex].upgradeLevel, level);
        playerStats.OnUpgrade(upgradeIndex);
    }

    public Sprite GetFishImage(int index)
    {
        return listFishes.fishData[index].fishIcon;
    }

    public string GetFishName(int index)
    {
        return listFishes.fishData[index].fishName;
    }

    public GameObject GetFish3D(int index)
    {
        return listFishes.fishData[index].fish3D;
    }

    public bool IsOnSound
    {
        get
        {
            return PlayerPrefsExtension.GetBool(PlayerPrefKeys.SOUND_STATUS, true);
        }
        set
        {
            PlayerPrefsExtension.SetBool(PlayerPrefKeys.SOUND_STATUS, value);
        }
    }

    public bool IsOnMusic
    {
        get
        {
            return PlayerPrefsExtension.GetBool(PlayerPrefKeys.MUSIC_STATUS, true);
        }
        set
        {
            PlayerPrefsExtension.SetBool(PlayerPrefKeys.MUSIC_STATUS, value);
        }
    }

    public bool IsOnVibration
    {
        get
        {
            return PlayerPrefsExtension.GetBool(PlayerPrefKeys.VIBRATION_STATUS, true);
        }
        set
        {
            PlayerPrefsExtension.SetBool(PlayerPrefKeys.VIBRATION_STATUS, value);
        }
    }

    public bool IsJoystick
    {
        get
        {
            return PlayerPrefsExtension.GetBool(PlayerPrefKeys.JOYSTICK_STATUS, true);
        }
        set
        {
            PlayerPrefsExtension.SetBool(PlayerPrefKeys.JOYSTICK_STATUS, value);
        }
    }
    public bool IsHireCashier
    {
        get
        {
            return PlayerPrefsExtension.GetBool(PlayerPrefKeys.IS_HIRE_CASHIER, false);
        }
        set
        {
            PlayerPrefsExtension.SetBool(PlayerPrefKeys.IS_HIRE_CASHIER, value);
        }
    }
    public int CurrentMoneyOnTable
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.CURRENT_MONEY_ON_TABLE, 0);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.CURRENT_MONEY_ON_TABLE, value);
        }
    }

    public int maxCustommerSceneHave()
    {
        int maxCustommer = 0;

        for (int i = 0; i < CustommerData.NumberCustommerDataList.Count; i++)
        {
            if (CustommerData.NumberCustommerDataList[i].UsedCoin <= UsedCoin)
            {
                maxCustommer = CustommerData.NumberCustommerDataList[i].NumberOfCustommer;
            }
        }
        return maxCustommer;
    }

    public void UnlockTankLevel(int level)
    {
        PlayerPrefsExtension.SetBool(PlayerPrefKeys.FISH_TANK + level, false);
        WorldLevel++;
    }
    public bool CheckIsTankUnlocked(int level)
    {
        return PlayerPrefsExtension.GetBool(PlayerPrefKeys.FISH_TANK + level, false);
    }
    // Start is called before the first frame update
    void Start()
    {
        defaultTutorial = 1;
        //PlayerPrefs.DeleteAll();
    }
    private void Update()
    {
        test();
    }
    void test()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            DataManager.Ins.WorldLevel = DataManager.Ins.WorldLevel;
        }
    }
    #region Spagetty
    int CapacityClaim
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.CAPACITY_CLAIM, listClaimUpgrade[0]);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.CAPACITY_CLAIM, value);
            OnClaimChange?.Invoke();
        }
    }
    int DiveClaim
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.DIVE_CLAIM, listClaimUpgrade[1]);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.DIVE_CLAIM, value);
            OnClaimChange?.Invoke();
        }
    }
    int SpeedClaim
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.SPEED_CLAIM, listClaimUpgrade[2]);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.SPEED_CLAIM, value);
            OnClaimChange?.Invoke();
        }
    }
    int WorkerClaim
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.WORKER_CLAIM, listClaimUpgrade[3]);
        }
        set
        {
            PlayerPrefs.SetInt(PlayerPrefKeys.WORKER_CLAIM, value);
            OnClaimChange?.Invoke();
        }
    }
    public int ClaimGet(int index)
    {
        switch (index)
        {
            case 3:
                return WorkerClaim;
            case 2:
                return SpeedClaim;
            case 1:
                return DiveClaim;
            default:
                return CapacityClaim;
        }
    }

    public void SetClaimUpgrade(int upgradeIndex, int index)
    {
        switch (upgradeIndex)
        {
            case 3:
                PlayerPrefs.SetInt("WORKER_CLAIM", index);
                break;
            case 2:
                PlayerPrefs.SetInt("SPEED_CLAIM", index);
                break;
            case 1:
                PlayerPrefs.SetInt("DIVE_CLAIM", index);
                break;
            default:
                PlayerPrefs.SetInt("CAPACITY_CLAIM", index);
                break;
        }
    }
    #endregion
}
