using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyerGotoPurchaseState : BotBaseState
{
    public override void EnterState(BuyerBot buyer)
    {
        TargetNearestPos(buyer);
    }
    void TargetNearestPos(BuyerBot buyer)
    {
        Transform target = GameController.Ins.CashierTable.TargetNearestAblePos(buyer).Pos;
        buyer.TargetPos = target;
        buyer.botMover.SetDestination(target.position);
        buyer.RunWithFishInHead();
    }
    public override void UpdateState(BuyerBot buyer)
    {
        if (IsNearTarget(buyer.transform, buyer.TargetPos))
        {
            buyer.SwitchState(buyer.queueForPurchaseState);
        }
    }
    bool IsNearTarget(Transform from, Transform to)
    {
        var distant = Vector3.Distance(from.position, to.position);
        if (distant < 0.1f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public override void OnTriggerEnter(BuyerBot buyer, Collider other)
    {

    }
    public override void ExitState(BuyerBot buyer)
    {
        buyer.IdleWithFishInHead();
    }
}
