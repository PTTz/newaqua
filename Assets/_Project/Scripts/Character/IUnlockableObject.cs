using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnlockableObject 
{
    public void Unlock();
}
