using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishAnimationController : MonoBehaviour
{
    Animator animator;

    float oldSpeed = 0;
    float newSpeed = 0;

    float smoothScale = 0.1f;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    private void Start()
    {
        
    }
    public void SetSwimSpeed(float speed)
    {
        newSpeed = speed;
    }

    public void Update()
    {
        oldSpeed = Mathf.Lerp(oldSpeed, newSpeed, smoothScale);
        animator.SetFloat("Speed", oldSpeed);
    }
}

