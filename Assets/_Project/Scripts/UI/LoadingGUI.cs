using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingGUI : MonoBehaviour
{
    [SerializeField] private Image LoadingBarFill;

    [SerializeField] Text loadingText;
    // Start is called before the first frame update
    void Start()
    {
        LoadingBarFill.fillAmount = 0f;
        StartCoroutine(AnimateLoadingText());
        StartCoroutine(WaitLoadData());
    }
    private void OnEnable()
    {
    }
    private IEnumerator WaitLoadData()
    {
        yield return LoadingBarFill.DOFillAmount(0.5f, 2f).SetEase(Ease.Linear);
        yield return new WaitUntil(() => GameController.Ins && GameController.Ins.IsDoneLoad);
        yield return LoadingBarFill.DOFillAmount(1f, 1f).SetEase(Ease.Linear).WaitForCompletion();
        gameObject.SetActive(false);
        //Close();
    }

    IEnumerator AnimateLoadingText()
    {
        while (true)
        {
            loadingText.text = "Loading";
            yield return new WaitForSeconds(0.8f);
            loadingText.text = "Loading.";
            yield return new WaitForSeconds(0.8f);
            loadingText.text = "Loading..";
            yield return new WaitForSeconds(0.8f);
            loadingText.text = "Loading...";
            yield return new WaitForSeconds(0.8f);
        }
    }
}
