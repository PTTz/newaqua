using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using TMPro;
using UCExtension.Common;
using UnityEngine;

public class BuyCashierZone : PlayerInteractionZone
{
    [SerializeField] int LevelToUnlock;
    [Header("Buy zone")]
    [SerializeField] int originalPrice;

    [SerializeField] int remainCoinToBuy;

    [Range(1, 50)]
    [SerializeField] int maxBuyStep;

    [SerializeField] TextMeshPro priceText;

    [SerializeField] Transform coinInput;

    [SerializeField] MovingObject moneyPrefab;

    [SerializeField] RecylableObject cashierObj;

    int CoinToBuy
    {
        get
        {
            return remainCoinToBuy;
        }
        set
        {
            remainCoinToBuy = value;
            priceText.text = $"{remainCoinToBuy}";
        }
    }
    void Start()
    {
        Load();
        DataManager.Ins.OnWorldLevelChange += OnWorldLevelChange;
        OnWorldLevelChange(DataManager.Ins.WorldLevel);
    }
    void OnWorldLevelChange(int worldLevel)
    {
        if (worldLevel >= LevelToUnlock)
        {
            UnlockWorkerZone();
        }
    }
    void UnlockWorkerZone()
    {
        gameObject.SetActive(true);
        transform.DOScale(0.3f, 0.2f);
    }
    public override void OnPlayerStay(Player player)
    {
        base.OnPlayerStay(player);
        if (CalculateCoin())
        {
            var spawnPos = player.transform.position;
            spawnPos.y += 1;
            var moneyObject = PoolObjects.Ins.SpawnObject<MovingObject>(moneyPrefab, spawnPos);
            moneyObject.Jump(coinInput.position, finish: () =>
            {
                moneyObject.Recyle();
            });
            if (CoinToBuy <= 0) UnLock();
            Save();
        }
    }

    void UnLock()
    {
        gameObject.transform.DOScale(Vector3.zero, 0.5f).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
        DataManager.Ins.IsHireCashier = true;
        PoolObjects.Ins.SpawnObject(cashierObj, transform.position);
    }

    bool CalculateCoin()
    {
        maxBuyStep = Mathf.Min(maxBuyStep, originalPrice);
        int moneyPerStep = originalPrice / maxBuyStep;
        moneyPerStep = Mathf.Min(moneyPerStep, remainCoinToBuy);
        if (moneyPerStep > 0 && DataManager.Ins.Coin >= moneyPerStep)
        {
            DataManager.Ins.Coin -= moneyPerStep;
            DataManager.Ins.UsedCoin += moneyPerStep;
            CoinToBuy -= moneyPerStep;
            return true;
        }
        return false;
    }
    public void Save()
    {
        PlayerPrefs.SetInt(PlayerPrefKeys.BUY_ZONE_REMAINNING_PRICE_COUNT + "CASHIER", CoinToBuy);
    }

    public void Load()
    {
        gameObject.SetActive(true);
        if (DataManager.Ins.IsHireCashier)
        {
            UnLock();
        }
        else
        {
            int remainingPrice = PlayerPrefs.GetInt(PlayerPrefKeys.BUY_ZONE_REMAINNING_PRICE_COUNT + "CASHIER", originalPrice);
            CoinToBuy = remainingPrice;
            if (CoinToBuy <= 0) UnLock();
            priceText.text = $"{remainCoinToBuy}";
        }
    }
}
