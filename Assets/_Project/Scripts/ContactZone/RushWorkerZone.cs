using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UCExtension.GUI;
using UnityEngine;

public class RushWorkerZone : MonoBehaviour
{
    [SerializeField] int LevelToUnlock;
    [SerializeField] public RushWorkerBot mainWorkerBot;
    //[SerializeField] HireWorkerZone hireWorkerZone;

    private void Start()
    {
        DataManager.Ins.OnWorldLevelChange += OnWorldLevelChange;
        OnWorldLevelChange(DataManager.Ins.WorldLevel);
        //GUIController.Ins.GetGUI("HireWorkerGUI").gameObject.GetComponent<WorkerGUI>().SetRushWorker(mainWorkerBot);
    }
    void OnWorldLevelChange(int worldLevel)
    {
        if (worldLevel >= LevelToUnlock)
        {
            UnlockWorkerZone();
        }
    }
    void UnlockWorkerZone()
    {
        gameObject.SetActive(true);
        transform.DOScale(1, 0.2f);
    }
}
