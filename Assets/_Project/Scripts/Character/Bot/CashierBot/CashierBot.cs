using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CashierBot : Character
{
    public Transform TargetPos;
    public BotMover botMover;
    bool Stay = false;

    bool isNearCashierTable = false;

    private void Start()
    {
        botMover.Warp();
        TargetPos = GameController.Ins.CashierTable.CashierStayPos;
    }
    private void Update()
    {
        if (!isNearCashierTable)
        {
            botMover.SetDestination(TargetPos.position);
            animationController.WalkingSpeed(3);
        }
        var isNearTarget = IsNearTarget(transform, TargetPos);
        if (isNearTarget && !Stay)
        {
            Stay = true;
            Vector3 oldPos = transform.position;
            botMover.SetDestination(oldPos + Vector3.right / 1.5f);
            isNearCashierTable = true;
            animationController.WalkingSpeed(0);
        }
    }
    bool IsNearTarget(Transform from, Transform to)
    {
        var distant = Vector3.Distance(from.position, to.position);
        if (distant < 0.1f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
