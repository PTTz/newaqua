using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TutorialLine : MonoBehaviour
{
    [SerializeField] LineRenderer line;
    [SerializeField] Transform player;
    [SerializeField] Material lineMat;
    [SerializeField] Material lineMatClone;

    public Vector3 endPos;
    // Start is called before the first frame update
    void Start()
    {
        lineMatClone = Instantiate(lineMat);
        line.material = lineMatClone;
        lineMatClone.SetTextureOffset("_MainTex", Vector2.zero);
        lineMatClone.DOOffset(new Vector2(-50f, 0), 20f).SetLoops(-1).SetEase(Ease.Linear);
    }

    // Update is called once per frame
    void Update()
    {
        line.SetPosition(0, new Vector3(player.position.x, player.position.y + 2, player.position.z));
        line.SetPosition(1, endPos);
    }

}
