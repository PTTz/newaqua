using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyTankZone : BuyZone
{
    public FishData FishData_;
    public int TankLevel;

    public override void UnLock()
    {
        base.UnLock();
        if (DataManager.Ins.CheckIsTankUnlocked(TankLevel))
        {
            return;
        }
        DataManager.Ins.UnlockTankLevel(TankLevel);

        lockedObject.SetActive(true);
        lockedObject.transform.localScale = Vector3.zero;
        lockedObject.transform.DOScale(Vector3.one, 0.5f);
        gameObject.transform.DOScale(Vector3.zero, 0.5f).OnComplete(() =>
        {
            if (TankLevel == 1)
            {
                GameController.Ins.TutorialUnlock(2);
            }
            if (TankLevel == 2)
            {
                GameController.Ins.TutorialUnlock(8);
            }
            gameObject.SetActive(false);
        });
    }
}
