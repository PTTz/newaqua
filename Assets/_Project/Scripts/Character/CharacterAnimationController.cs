using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
    Animator animator;

    AnimationEventTrigger eventTrigger;

    CharacterAnimState currentState;

    float oldSpeed = 0;
    float newSpeed = 0;

    int stateKeyHash = 0;

    int onChangeStateKeyHas = 0;

    int diveTypeKeyHash = 0;

    int HoldTypeKeyHash = 0;

    float smoothScale = 0.1f;

    private void Awake()
    {
        animator = GetComponent<Animator>();

        stateKeyHash = Animator.StringToHash(CharacterAnimationKey.StateType);
        HoldTypeKeyHash = Animator.StringToHash(CharacterAnimationKey.HoldType);
        onChangeStateKeyHas = Animator.StringToHash(CharacterAnimationKey.OnChangeState);
        diveTypeKeyHash = Animator.StringToHash(CharacterAnimationKey.DiveStyle);
        eventTrigger = GetComponent<AnimationEventTrigger>();
    }

    public void ChangeAnimation(CharacterAnimState state)
    {
        if (currentState == state) return;
        currentState = state;
        animator.SetInteger(stateKeyHash, (int)state);
        animator.SetTrigger(onChangeStateKeyHas);
    }

    public void Update()
    {
        oldSpeed = Mathf.Lerp(oldSpeed, newSpeed, smoothScale);
        animator.SetFloat("Speed", oldSpeed);

        if (Input.GetKeyDown(KeyCode.X))
        {
            DeepDive();
        }
    }

    public void WalkingSpeed(float newSpeed_)
    {
        ChangeAnimation(CharacterAnimState.Moving);
        newSpeed = newSpeed_;
        //animator.SetFloat("Speed", oldSpeed);
    }

    public void Dive(int dive)
    {
        //Todo : change Dive Type;
        ChangeAnimation(CharacterAnimState.Dive);
        animator.SetInteger(diveTypeKeyHash, dive);
    }
    public void DeepDive()
    {
        ChangeAnimation(CharacterAnimState.DeepDive);
    }
    public void Swim(float speed)
    {
        ChangeAnimation(CharacterAnimState.Swim);
        animator.SetFloat("Speed", speed);
    }

    public void HoldType(int holdType)
    {
        //todo : Hold Fish

        //holdType  = 0 : to idle hold fish
        //holdType  = 1 : to run hold fish
        ChangeAnimation(CharacterAnimState.HoldingFish);
        animator.SetInteger(HoldTypeKeyHash, holdType);
    }

}

public enum CharacterAnimState
{
    Moving,
    Fall,
    DeepDive,
    Dive,
    Swim,
    HoldingFish
}

public static class CharacterAnimationKey
{
    public static string DiveStyle = "DiveStyle";

    public static string StateType = "StateType";

    public static string SwimmingType = "SwimmingType";

    public static string HoldType = "HoldType";

    public static string OnChangeState = "OnChangeState";
}