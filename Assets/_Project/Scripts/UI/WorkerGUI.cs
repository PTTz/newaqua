using API.Ads;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UCExtension.GUI;
using UnityEngine;
using UnityEngine.UI;

public class WorkerGUI : BaseGUI
{
    #region Set Worker
    [SerializeField] WorkerBot workerBot;
    [SerializeField] RushWorkerBot rushWorkerBot;
    public bool rushOrNormalWorker = false;

    public void SetWorker(WorkerBot bot)
    {
        workerBot = bot;
    }
    public void SetRushWorker(RushWorkerBot bot)
    {
        rushWorkerBot = bot;
    }
    #endregion

    [SerializeField] Button claimButton;
    [SerializeField] Button exitButton;

    void OnEnable()
    {
        claimButton.gameObject.transform.localScale = Vector3.zero;
        claimButton.gameObject.transform.DOScale(1f, 0.5f).SetEase(Ease.OutBack);
    }

    void Start()
    {
        claimButton.onClick.AddListener(OnClickClaimWorkerAdsButton);
        exitButton.onClick.AddListener(OnClickExitButton);
    }

    void OnClickExitButton()
    {
        CloseUI(() =>
        {
            base.Close();
        });
    }

    void OnClickClaimWorkerAdsButton()
    {
        if (AdManager.Ins)
        {
            AdManager.Ins.ShowRewardedVideo("hireworker", (finished) =>
            {
                if (finished)
                {
                    Hire();
                }
            });
        }
        else
        {
            Hire();
        }
    }
    void Hire()
    {
        if (workerBot && rushOrNormalWorker == false)
        {
            workerBot.StartHired();
        }

        if (rushWorkerBot && rushOrNormalWorker == true)
        {
            rushWorkerBot.StartHire();
        }
        CloseUI(() =>
        {
            base.Close();
        });
    }
    void CloseUI(TweenCallback OnComplete = null)
    {
        Sequence sequence = DOTween.Sequence();

        sequence.Insert(0f, claimButton.gameObject.transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InBack));

        sequence.OnComplete(OnComplete);
    }
}
