using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UCExtension.Common;

public class RushWorkerBot : RecylableObject
{
    [SerializeField] List<WorkerBot> workers;
    public void StartHire()
    {
        foreach (var worker in workers)
        {
            worker.StartHired();
        }
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.H))
        {
            StartHire();
        }
    }
}
