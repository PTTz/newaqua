using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UCExtension.GUI;

public class PlayerUpgrade : PlayerInteractionZone
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnPlayerEnter(Player player)
    {
        base.OnPlayerEnter(player);
        GUIController.Ins.ShowGUI("UpgradeGUI");
        GameController.Ins.TutorialUnlock(9);
    }

    public override void OnPlayerExit(Player player)
    {
        base.OnPlayerExit(player);
        GUIController.Ins.HideGUI("UpgradeGUI");
    }
}
