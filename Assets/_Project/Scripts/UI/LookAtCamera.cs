//using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    [SerializeField] Camera mainCam;
    // Start is called before the first frame update
    void Start()
    {
        mainCam = GameController.Ins.Camera_;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(transform.position + mainCam.transform.rotation * Vector3.forward, mainCam.transform.rotation * Vector3.up);
    }
}
