using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : IdentifierObject
{
    public CharacterAnimationController animationController;

    [SerializeField] protected StackCollector stackCollector;

    public StackCollector Collector => stackCollector;

    protected override void Awake()
    {
        RegisterEvent();
    }

    // Update is called once per frame
    void Update()
    {

    }

    protected virtual void RegisterEvent()
    {
        //stackCollector.OnCollectObject += OnCollectObject;
        //stackCollector.OnEmptyObject += OnEmptyObject;
    }

    //void OnCollectObject()
    //{
    //    animationController.ChangeCarryingState(true);
    //}
    //void OnEmptyObject()
    //{
    //    animationController.ChangeCarryingState(false);
    //}
    #region Save And Load

    protected override void Save()
    {
        base.Save();
        SavePosition();
    }

    protected override void Load()
    {
        base.Load();
        LoadPosition();
    }
    void LoadPosition()
    {
        //transform.position = ES3.Load<Vector3>(DataKeys.PLAYER_POSITION + Identifier.ID, transform.position);
    }
    protected void SavePosition()
    {
        //ES3.Save(DataKeys.PLAYER_POSITION + Identifier.ID, transform.position);
    }
    #endregion
}
