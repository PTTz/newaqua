using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefKeys
{
    public const string COIN = "COIN";

    public const string COIN_CLAIMED = "COIN_CLAIMED";

    public const string CURRENT_MONEY_ON_TABLE = "CURRENT_MONEY_ON_TABLE";

    public const string SHOW_FULLED = "SHOW_FULLED";

    public const string IS_HIRE_CASHIER = "IS_HIRE_CASHIER";

    public const string BUY_ZONE_REMAINNING_PRICE_COUNT = "BUY_ZONE_REMAINNING_PRICE_COUNT";

    public const string FISH_TANK = "FISH_TANK";

    public const string USED_COIN = "USED_COIN";

    public const string WORLD_LEVEL = "WORLD_LEVEL";

    public const string JOYSTICK_STATUS = "JOYSTICK_STATUS";

    public const string VIBRATION_STATUS = "VIBRATION_STATUS";

    public const string MUSIC_STATUS = "MUSIC_STATUS";

    public const string SOUND_STATUS = "SOUND_STATUS";

    public const string CAPACITY_LEVEL = "CAPACITY_LEVEL";

    public const string SPEED_LEVEL = "SPEED_LEVEL";

    public const string DIVE_LEVEL = "DIVE_LEVEL";

    public const string WORKER_LEVEL = "WORKER_LEVEL";

    public const string CAPACITY_CLAIM = "CAPACITY_CLAIM";

    public const string SPEED_CLAIM = "SPEED_CLAIM";

    public const string DIVE_CLAIM = "DIVE_CLAIM";

    public const string WORKER_CLAIM = "WORKER_CLAIM";

    public const string UNLOCKED_TUTORIAL = "UNLOCKED_TUTORIAL";
}
