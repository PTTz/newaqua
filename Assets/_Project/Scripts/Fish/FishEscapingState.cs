using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishEscapingState : BaseState<FishController>
{
    public override void Enter()
    {
        base.Enter();
        owner.BotMover_.SpeedChange(owner.Fish.EscapingSpeed);
        if(owner.FishAnimationController_)
        {
            owner.FishAnimationController_.SetSwimSpeed(owner.Fish.EscapingSpeed);
        }
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
        EscapeToPos();
        owner.BotMover_.SetDestination(owner.TargetPos);
    }
    void EscapeToPos()
    {
        float x = owner.hunterTranform.position.x;
        float z = owner.hunterTranform.position.z;
        owner.TargetPos = new Vector3(-x, 0, -z);
    }
    public override void Exit()
    {
        base.Exit();
        owner.BotMover_.SpeedChange(owner.Fish.NormalSpeed);
        if(owner.FishAnimationController_)
        {
            owner.FishAnimationController_.SetSwimSpeed(owner.Fish.NormalSpeed);
        }
    }
}
