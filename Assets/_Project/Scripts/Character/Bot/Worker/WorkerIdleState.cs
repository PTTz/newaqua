using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerIdleState : BaseState<WorkerBot>
{
    public override void Enter()
    {
        base.Enter();
        owner.fishTargeted = null;
        owner.tankTargeted = null;
        owner.botMover.SetDestination(owner.Home.position);
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
        //if(owner.tankTargeted.FishOnTankList.Count < owner.tankTargeted.MaxTankCapacity)
        //{
        //    if(owner.carringFishList.Count > 0)
        //    {
        //        owner.stateMachine.ChangeState<WorkerBringFishToTankState>();
        //    }
        //}
        if (IsNearTarget(owner.transform, owner.Home, owner.NearHomeValue))
        {
            owner.animationController.WalkingSpeed(0);
            owner.botMover.SetDestination(owner.transform.position);
        }
        else
        {
            owner.animationController.WalkingSpeed(owner.walkAnimSpeed);
            owner.botMover.SetDestination(owner.Home.position);
        }
    }
    public bool IsNearTarget(Transform from, Transform to, float isSmallerThisFloat)
    {
        float distant = Vector3.Distance(from.position, to.position);
        if (distant > isSmallerThisFloat)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public override void Exit()
    {

        base.Exit();
    }
}
