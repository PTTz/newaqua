using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class DiveZone : MonoBehaviour
{
    //[SerializeField] private BoxCollider diveCollider;
    [SerializeField] private float diveTime = 2.0f;
    [SerializeField] private Animation diveboardAnim;
    [SerializeField] private CharacterAnimationController playerVisualAnim;
    [SerializeField] private Transform diveboardEndPosTransform;
    [SerializeField] private int diveStyle;
    public int diveLevel;
    [SerializeField] private List<AnimationCurve> easingDiving;

    [SerializeField] private AnimationCurve customEasingDiving;
    //[SerializeField] private EaseFunction customEase;

    Vector3 customDivePosition;

    void Awake()
    {
        
        //diveCollider = GetComponent<BoxCollider>();
        //diveboardAnim = GetComponent<Animation>();
        //diveboardTransform = gameObject.transform.GetChild(0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals(TagName.Player))
        {
            //print("Gia PHUC");

            diveStyle = UnityEngine.Random.Range(0, diveLevel + 1);
            //diveStyle = 4;

            diveboardAnim.Play("DiveBoard");
            var charController = other.GetComponent<Player>();
            playerVisualAnim = charController.gameObject.transform.GetChild(0).gameObject.transform.GetChild(1).GetComponent<CharacterAnimationController>();
            var charNavAgent = other.GetComponent<NavMeshAgent>();
            switch (diveStyle)
            {
                case 4:
                    CustomDiving(diveTime * 0.85f, 10f, new Vector3(1.5f, 3.5f, 0f), charController, charNavAgent, easingDiving[4], other, diveStyle);
                    break;
                case 3:
                    CustomDiving(diveTime * 0.85f, 10f, new Vector3(-2.5f, 2.5f, 0f), charController, charNavAgent, easingDiving[3], other, diveStyle);
                    break;
                case 2:
                    CustomDiving(diveTime * 0.75f, 6.5f, new Vector3(5f, 1.5f, 0f), charController, charNavAgent, easingDiving[2], other, diveStyle);
                    break;
                case 1:
                    CustomDiving(diveTime * 0.85f, 15f, new Vector3(2.0f, 1.5f, 0f), charController, charNavAgent, easingDiving[1], other, diveStyle);
                    break;
                default:
                    CustomDiving(diveTime * 0.95f, 12f, new Vector3(2f, 1.5f, 0f), charController, charNavAgent, easingDiving[0], other, diveStyle);
                    break;
            }
        }
    }

    private void CustomDiving(float time,
        float jumpForce,
        Vector3 adjustPosition,
        Player charController,
        NavMeshAgent charNavAgent,
        AnimationCurve easingDiving,
        Collider other,
        int diveStyle
        )
    {
        float skipDistance = UnityEngine.Random.Range(4f, 7f);

        customEasingDiving = ScaleCurve(easingDiving, time * 0.85f, 1f);
        customDivePosition = diveboardEndPosTransform.position - adjustPosition;
        charController.DivingWithSytle();
        playerVisualAnim.Dive(diveStyle);
        charNavAgent.enabled = !charNavAgent.enabled;
        Vector3 skipPosition = customDivePosition + new Vector3(skipDistance, adjustPosition.y, 0f);

        DOTween.Sequence()
        .Append(charController.gameObject.transform.DOJump(customDivePosition, jumpForce, 1, time * 0.85f).SetEase(Ease.Linear))
        //.Join(charController.gameObject.transform.DORotate(Vector3.zero, time * 0.85f))
        //.Append(playerVisualAnim.gameObject.transform.DOJump(playerVisualAnim.gameObject.transform.position, -1.0f, 1, time * 0.75f))
        .Append(charController.gameObject.transform.DOJump(
            skipPosition,
            jumpForce * -0.15f,
            1,
            time * 0.35f).SetEase(Ease.OutSine)
        );

        StartCoroutine(UpdateNavMeshAgent(charNavAgent, charController, time, adjustPosition, skipDistance));
    }

    IEnumerator UpdateNavMeshAgent(NavMeshAgent charNavAgent, Player charController,float customdiveTime, Vector3 adjustPosition, float skipDistance)
    {
        yield return new WaitForSeconds(customdiveTime * 1.2f);
        charNavAgent.Warp(customDivePosition + new Vector3(skipDistance, adjustPosition.y, 0f));
        charController.DivingWithSytle();
        yield return new WaitForSeconds(customdiveTime * 0.1f);
        charNavAgent.enabled = !charNavAgent.enabled;
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    print("Gia PHUC 2");
    //}
    //private void OnTriggerExit(Collider other)
    //{
    //    print("Gia PHUC 3");
    //}

    public static AnimationCurve ScaleCurve(AnimationCurve curve, float maxX, float maxY)
    {
        AnimationCurve scaledCurve = new AnimationCurve();
        for (int i = 0; i < curve.keys.Length; i++)
        {
            Keyframe keyframe = curve.keys[i];
            keyframe.value = curve.keys[i].value * maxY;
            keyframe.time = curve.keys[i].time * maxX;
            keyframe.inTangent = curve.keys[i].inTangent * maxY / maxX;
            keyframe.outTangent = curve.keys[i].outTangent * maxY / maxX;

            scaledCurve.AddKey(keyframe);
        }
        return scaledCurve;
    }
}
