using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Player : Character
{
    public int ATK;

    public int MaxBaloSize;

    [SerializeField] GameObject maxIcon;

    public List<FishController> carringFishList;

    [SerializeField] float changeSpeed;

    [SerializeField] HomeGUI homeGUI;

    [SerializeField] Transform collectPosition;

    [SerializeField] Cinemachine.CinemachineVirtualCamera cameraVC;

    [SerializeField] PlayerMover playerMover;
    
    NavMeshAgent playerAgent;

    [SerializeField] FishColCatch fishColCatch;

    [Header("Player")]

    [SerializeField] Transform baloPos;

    [SerializeField] Transform playerSkinTransform;

    [SerializeField] List<GameObject> playerSkin;

    [SerializeField] Transform wharfReturnTransform;
    
    [Header("Boat")]

    [SerializeField] Transform boatBaloPos;

    [SerializeField] Transform shipSkin;

    [SerializeField] GameObject playerShipSkin;

    [SerializeField] Transform shipWharfTransform;

    [Header("Else")]
    [SerializeField] NavMeshAgent playerNavMesh;

    [SerializeField] CharacterAnimationController playerAnim;

    float speed;

    float targetSpeed;

    bool isSwimming;

    bool isDiving;

    bool isBoating;

    bool isCoolDownBoat;

    protected override void Awake()
    {
        base.Awake();
        maxIcon.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        playerAgent = GetComponent<NavMeshAgent>();
        ControlPositionJoyStick.OnChangeMoveInput += OnChangeMovementInput;
        fishColCatch.OnColCatchFish += CatchedNewFish;
        fishColCatch.SetOwnerColCatch(this);
    }

    public FishController GetFish(FishType type)
    {
        foreach (var fish in carringFishList)
        {
            if (fish.Fish.FishType_ == type)
            {
                carringFishList.Remove(fish);
                return fish;
            }
        }
        return null;
    }
    public void DivingWithSytle()
    {
        isDiving = !isDiving;
        speed = 0;
    }
    void OnChangeMovementInput(Vector2 movementInput)
    {
        if (!isDiving)
        {
            if (Mathf.Abs(movementInput.x) > 0.01f || Mathf.Abs(movementInput.y) > 0.01f)
            {
                playerMover.OnChangeMovementInput(movementInput);
                targetSpeed = movementInput.magnitude / 2f;
            }
            else
            {
                targetSpeed = 0;
            }
            speed = Mathf.Lerp(speed, targetSpeed, changeSpeed);
            if (isSwimming)
            {
                animationController.Swim(speed);
            }
            else
            {
                animationController.WalkingSpeed(speed);
            }
        }
        else
        {
            speed = 0;
        }
        //SavePosition();
    }

    private void OnTriggerEnter(Collider other)
    {
        var interactableObject = other.GetComponent<PlayerInteractionZone>();
        if (interactableObject)
        {
            interactableObject.OnPlayerEnter(this);
        }

        if (other.tag.Equals(TagName.FishTankStayZone))
        {
            ThrowFishToTank(other);
        }

        if (isBoating == true)
        {
            if (other.tag.Equals(TagName.UnShip) && isCoolDownBoat == false)
            {
                SwitchingBackFromBoat();
            }
        }
        else
        {
            if (other.tag.Equals(TagName.Ship))
            {
                SwitchingToBoat();
                StartCoroutine(BoatCoolDown());
            }
        }
        if (other.tag.Equals(TagName.Water))
        {
            isSwimming = true;
            fishColCatch.gameObject.SetActive(true);
        }
        IEnumerator BoatCoolDown()
        {
            isCoolDownBoat = true;
            yield return new WaitForSeconds(1.5f);
            isCoolDownBoat = false;
        }
    }

    private void SwitchingToBoat()
    {
        isBoating = !isBoating;

        playerAgent.Warp(shipSkin.transform.GetChild(0).position);
        playerAgent.agentTypeID = GetAgenTypeIDByName("Boat");
        playerAgent.radius = 3;
        //Balance Speed When Boat/Ship playerAgent.speed = ???

        shipSkin.transform.SetParent(playerSkinTransform);

        //Player rotation.y = boat rotation.y
        shipSkin.localRotation = Quaternion.identity;

        foreach (GameObject gameObject in playerSkin) gameObject.SetActive(false);
        playerShipSkin.SetActive(true);
        //fishColCatch.gameObject.SetActive(false);
        fishColCatch.transform.localPosition = fishColCatch.transform.localPosition + new Vector3(-0.25f, 0f, 2.25f);
    }
    private void SwitchingBackFromBoat()
    {
        isBoating = !isBoating;
        //Balance Speed When Back From Boat

        shipSkin.transform.SetParent(shipWharfTransform);

        playerAgent.Warp(wharfReturnTransform.position);
        playerAgent.agentTypeID = GetAgenTypeIDByName("Humanoid");
        playerAgent.radius = 0.5f;

        foreach (GameObject gameObject in playerSkin) gameObject.SetActive(true);
        playerShipSkin.SetActive(false);
        fishColCatch.transform.localPosition = fishColCatch.transform.localPosition - new Vector3(-0.25f, 0f, 2.25f);
    }
    public void DeepDive()
    {
        playerAnim.DeepDive();
        playerAnim.transform.DOLocalMoveY(-4, 1f).OnComplete(() =>
        {
            playerAnim.transform.DOLocalMoveY(0, 1f);
            //playerAnim.transform.DORotate()
        });
    }
    public void CollectProduct(MovingObject product)
    {
        product.transform.SetParent(transform);
        //product.OnPlayerCollect();
        product.LocalRecyleJump(collectPosition.localPosition);
    }
    void ThrowFishToTank(Collider other)
    {
        var tank = other.GetComponent<FishTank>();
        FishType tankFishType = tank.defaultFish.Fish.FishType_;
        Transform tankCenter = tank.tankCenter;
        maxIcon.SetActive(false);
        foreach (var fish in carringFishList.ToList())
        {
            if (fish.Fish.FishType_ == tankFishType)
            {
                fish.Jump(tankCenter.position, 0.5f, finish: () => 
                {
                    fish.ToTheTank();
                });
                fish.transform.DOScale(0.3f, 0.6f);
                fish.transform.parent = other.transform;
                carringFishList.Remove(fish);
                GameController.Ins.PushNewFishToTank(fish);
                tank.FishOnTankList.Add(fish);
                homeGUI.OnRemoveFish(fish.Fish.FishLevel);
            }
        }
        GameController.Ins.TutorialUnlock(5);
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals(TagName.Water) && isBoating != true)
        {
            isSwimming = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var interactableObject = other.GetComponent<PlayerInteractionZone>();
        if (interactableObject)
        {
            interactableObject.OnPlayerExit(this);
        }
        if (other.tag.Equals(TagName.Water) && isBoating != true)
        {
            isSwimming = false;
            fishColCatch.gameObject.SetActive(false);
        }
    }
    void CatchedNewFish(FishController newFish, Character charCatchFish)
    {
        if (charCatchFish != this) return;
        foreach (var fish in carringFishList)
        {
            if (fish == newFish) return;
        }
        carringFishList.Add(newFish);
        GameController.Ins.fishAbleToCatchList.Remove(newFish);
        newFish.gameObject.transform.parent = baloPos;
        newFish.Jump(baloPos.position);
        newFish.transform.DOScale(0.01f, 0.6f);
        homeGUI.OnCatched(newFish.Fish.FishLevel);
        if (carringFishList.Count >= MaxBaloSize)
        {
            maxIcon.SetActive(true);
            fishColCatch.gameObject.SetActive(false);
            GameController.Ins.TutorialUnlock(3);
        }
    }

    private int GetAgenTypeIDByName(string agentTypeName)
    {
        int count = NavMesh.GetSettingsCount();
        string[] agentTypeNames = new string[count + 2];
        for (var i = 0; i < count; i++)
        {
            int id = NavMesh.GetSettingsByIndex(i).agentTypeID;
            string name = NavMesh.GetSettingsNameFromID(id);
            if (name == agentTypeName)
            {
                return id;
            }
        }
        return -1;
    }
    private void OnDisable()
    {
        Debug.Log("PLAYER DIS");
    }
}
