using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UCExtension.Common;
using UnityEngine;
using UnityEngine.Events;

public class MovingObject : RecylableObject
{
    const float timeScaleUp = 0.5f;

    Tween movingTween;

    Tween scaleTween;

    protected void ScaleUp()
    {
        scaleTween?.Kill();
        transform.localScale = Vector3.zero;
        scaleTween = transform.DOScale(Vector3.one, timeScaleUp);
    }

    public void WolrRecyleJump(Vector3 targetPosition, float time = 0.5f)
    {
        Jump(targetPosition, time, () =>
        {
            Recyle();
        });
        scaleTween?.Kill();
        scaleTween = transform.DOScale(Vector3.one * 0.5f, time).SetEase(Ease.Linear);
    }
    public void LocalRecyleJump(Vector3 targetPosition, float time = 0.5f)
    {
        LocalJump(targetPosition, time, () =>
        {
            Recyle();
        });
        scaleTween?.Kill();
        scaleTween = transform.DOScale(Vector3.one * 0.5f, time).SetEase(Ease.Linear);
    }


    public void ScaleDownAndRecyle(float time = 0.5f)
    {
        scaleTween?.Kill();
        scaleTween = transform.DOScale(Vector3.zero, time).OnComplete(() =>
        {
            Recyle();
        });
    }
    public void SetLocalPosition(Vector3 targetPosition)
    {
        scaleTween?.Kill();
        movingTween?.Kill();
        transform.localPosition = targetPosition;
    }
    public void SetPosition(Vector3 targetPosition)
    {
        scaleTween?.Kill();
        movingTween?.Kill();
        transform.position = targetPosition;
    }
    public void Move(Vector3 targetPosition, float time = 0.2f, UnityAction finish = null)
    {
        movingTween?.Kill();
        movingTween = transform.DOMove(targetPosition, time).OnComplete(() =>
        {
            finish?.Invoke();
        });
    }
    public void LocalMove(Vector3 targetPosition, float time = 0.2f)
    {
        movingTween?.Kill();
        movingTween = transform.DOLocalMove(targetPosition, time);
    }
    public void LocalJump(Vector3 targetPosition, float time = 0.5f, UnityAction finish = null)
    {
        float jumpForce = 5;
        movingTween?.Kill();
        movingTween = transform.DOLocalJump(targetPosition, jumpForce, 1, time).OnComplete(() =>
        {
            finish?.Invoke();
        });
    }
    public void Jump(Vector3 targetPosition, float time = 0.5f, UnityAction finish = null)
    {
        float jumpForce = 5;
        movingTween?.Kill();
        movingTween = transform.DOJump(targetPosition, jumpForce, 1, time).OnComplete(() =>
        {
            finish?.Invoke();
        });
    }
}
