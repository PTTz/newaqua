using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class BotMover : NavMeshAgentController
{
    [SerializeField] float walkRadius;
    [SerializeField] float speed;
    public Action OnStopMove;

    public Action OnStartMove;

    bool isMoving;

    private void Update()
    {
        bool isNavmeshMoving = navMeshAgent.velocity.magnitude > 0.1f;
        if (!isNavmeshMoving && isMoving)
        {
            OnStopMove?.Invoke();
        }
        if (isNavmeshMoving && !isMoving)
        {
            OnStartMove?.Invoke();
        }
        isMoving = isNavmeshMoving;
    }
    public void SpeedChange(float newSpeed)
    {
        navMeshAgent.speed = newSpeed;
    }
    public void EnabedNavMesh(bool isActive)
    {
        navMeshAgent.enabled = isActive;
    }
    public void Warp()
    {
        navMeshAgent.Warp(transform.position);
    }
    public void SetDestination(Vector3 targetPosition)
    {
        navMeshAgent.SetDestination(targetPosition);
    }
    public void MoveToRandomPosition()
    {
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * walkRadius;
        randomDirection += transform.position;
        navMeshAgent.SetDestination(GetReachablePosition(randomDirection));
    }

    public Vector3 GetReachablePosition(Vector3 position)
    {
        NavMeshHit hit;
        NavMesh.SamplePosition(position, out hit, walkRadius, 1);
        Vector3 finalPosition = hit.position;
        return finalPosition;
    }
}
