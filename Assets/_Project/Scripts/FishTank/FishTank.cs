using System.Collections;
using System.Collections.Generic;
using UCExtension;
using UCExtension.Common;
using UnityEngine;

public class FishTank : MonoBehaviour
{
    public FishController defaultFish;
    public int MaxTankCapacity;
    public List<FishController> FishOnTankList;
    public Transform tankCenter;
    [SerializeField] string playerPrefKeys;
    [SerializeField] SpriteRenderer FishIconOfTank;
    Player player;

    int fishCount
    {
        get 
        { 
            return PlayerPrefs.GetInt(playerPrefKeys,0); 
        }
        set 
        { 
            PlayerPrefs.SetInt(playerPrefKeys, value);
        }
    }
    private void Awake()
    {
        FishIconOfTank.sprite = defaultFish.Fish.fishIcon;
    }
    private void Start()
    {
        for (int i = 0; i < fishCount; i++)
        {
            var newFish = PoolObjects.Ins.SpawnObject(defaultFish, tankCenter);
            var newFish_ = newFish.GetComponent<FishController>();
            FishOnTankList.Add(newFish_);
            GameController.Ins.PushNewFishToTank(newFish_);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals(TagName.Player))
        {
            player = other.GetComponent<Player>();
        }
    }
    private void OnTriggerStay(Collider other)
    {

    }
    private void OnTriggerExit(Collider other)
    {

    }
    private void OnDestroy()
    {
        fishCount = FishOnTankList.Count;
    }
    public FishController OnBuyerPickFish(BuyerBot buyer)
    {
        return FishOnTankList.PopRandomElement();
    }
}
