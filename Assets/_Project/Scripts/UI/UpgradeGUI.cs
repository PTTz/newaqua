﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UCExtension.GUI;


public class UpgradeGUI : BaseGUI
{
    [SerializeField] Button closeButton;

    [SerializeField] List<GameObject> choices;

    [SerializeField] ListUpgrade listUpgrades;

    [SerializeField] GameObject choicePrefab;

    [SerializeField] Transform listChoice;

    [SerializeField] Animator upgradeAnim;

    void Start()
    {
        closeButton.onClick.AddListener(Close);
        SetUpListUpgrade();
    }

    void SetUpListUpgrade()
    {
        for (int i = 0; i < listUpgrades.upgradeData.Length; i++)
        {
            var upgrade = Instantiate(choicePrefab);
            choices.Add(upgrade);
            //lấy thứ tự trong list để set cho skill
            choices[i].GetComponent<ChoiceUpgrade>().upgradeIndex = i;
            //set parent để reset lại vị trí
            choices[i].transform.SetParent(listChoice);
        } 
    }
    public void Close()
    {
        base.Close();
    }

    void OnEnable()
    {
        upgradeAnim.Play("UpgradePopUp");    
    }
}
