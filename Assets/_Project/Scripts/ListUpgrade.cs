using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ListUpgrade", menuName = "Create List Upgrade")]
public class ListUpgrade : ScriptableObject
{
    public UpgradeData[] upgradeData;
}
