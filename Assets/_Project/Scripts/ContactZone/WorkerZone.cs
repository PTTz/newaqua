using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UCExtension.GUI;
using UnityEngine;

public class WorkerZone : MonoBehaviour
{
    [SerializeField] int LevelToUnlock;
    [SerializeField] public WorkerBot mainWorkerBot;
    [SerializeField] Transform workerGraphic;
    //[SerializeField] HireWorkerZone hireWorkerZone;

    private void Start()
    {
        DataManager.Ins.OnWorldLevelChange += OnWorldLevelChange;
        OnWorldLevelChange(DataManager.Ins.WorldLevel);
        //GUIController.Ins.GetGUI("HireWorkerGUI").gameObject.GetComponent<WorkerGUI>().SetWorker(mainWorkerBot);
    }
    void OnWorldLevelChange(int worldLevel)
    {
        if(worldLevel >= LevelToUnlock)
        {
            UnlockWorkerZone();
        }
    }
    void UnlockWorkerZone()
    {

        workerGraphic.DOScale(1, 0.5f).OnComplete(() => 
        {
        });
    }
}
