using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagName 
{
    public const string DoorWay = "DoorWay";

    public const string Cashier = "Cashier";

    public const string Player = "Player";

    public const string Worker = "Worker";

    public const string HookAble = "Hookable";

    public const string Water = "Water";

    public const string FishTank = "FishTank";

    public const string MoneyZone = "MoneyZone";

    public const string FishTankStayZone = "FishTankStayZone";

    public const string CustomerWalkZone = "CustomerWalkZone";

    public const string CusTommerStopZone = "CusTommerStopZone";

    public const string Ship = "Ship";

    public const string UnShip = "UnShip";
}
