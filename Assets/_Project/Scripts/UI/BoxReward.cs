using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UCExtension.GUI;

public class BoxReward : PlayerInteractionZone
{
    [SerializeField] int id;
    public override void OnPlayerEnter(Player player)
    {
        base.OnPlayerEnter(player);
        switch (id)
        {
            case 3:
                GUIController.Ins.ShowGUI("TreasureRewardGUI");
                break;
            case 2:
                GUIController.Ins.ShowGUI("ShellRewardGUI");
                break;
            case 1:
                GUIController.Ins.ShowGUI("SuitCaseRewardGUI");
                break;
            default:
                Debug.LogError("BRUHHH check yo reward ID");
                break;
        }
        
    }

    public override void OnPlayerExit(Player player)
    {
        base.OnPlayerExit(player);
        switch (id)
        {
            case 3:
                GUIController.Ins.HideGUI("TreasureRewardGUI");
                break;
            case 2:
                GUIController.Ins.HideGUI("ShellRewardGUI");
                break;
            case 1:
                GUIController.Ins.HideGUI("SuitCaseRewardGUI");
                break;
            default:
                Debug.LogError("BRUHHH check yo reward ID");
                break;
        }
    }
}
