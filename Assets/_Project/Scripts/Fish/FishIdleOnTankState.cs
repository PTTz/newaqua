using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishIdleOnTankState : BaseState<FishController>
{

    float interval = 0.5f;
    float nextTime = 0;
    public override void Enter()
    {
        base.Enter();
        owner.BotMover_.SpeedChange(owner.Fish.NormalSpeed);
        owner.TargetPos = owner.transform.position;
        TargetNewPos();
        Debug.Log("ON FISH TANK STATE");
    }
    void PosControll()
    {
        if (Time.time >= nextTime)
        {
            TargetNewPos();
            nextTime += interval;
        }
    }
    void TargetNewPos()
    {
        float newX = UnityEngine.Random.Range(-10, 10);
        float newZ = UnityEngine.Random.Range(-10, 10);
        Vector3 offset = new Vector3(newX, 0, newZ);
        owner.TargetPos = owner.transform.position + offset;
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
        PosControll();
        owner.BotMover_.SetDestination(owner.TargetPos);

        if (owner.IsNearTarget(owner.transform.position, owner.TargetPos, 2f))
        {
            TargetNewPos();
        }
    }
    public override void Exit()
    {
        base.Exit();
    }
}
