using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerStats : MonoBehaviour
{
    [SerializeField] Player playerCapacity;
    [SerializeField] public PlayerMover playerSpeed;
    [SerializeField] DiveZone diveZone1;
    [SerializeField] DiveZone diveZone2;
    [SerializeField] WorkerBot worker;
    // Start is called before the first frame update
    void Start()
    {
        playerCapacity.MaxBaloSize += DataManager.Ins.GetUpgradeLevel(0);
        playerSpeed.baseMovementSpeed += (playerSpeed.speedIncrease * DataManager.Ins.GetUpgradeLevel(2));
        diveZone1.diveLevel = DataManager.Ins.GetUpgradeLevel(1);
        diveZone2.diveLevel = DataManager.Ins.GetUpgradeLevel(1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnUpgrade(int index)
    {
        switch (index)
        {
            case 0:
                playerCapacity.MaxBaloSize += 1;
                break;
            case 1:
                if (diveZone1.diveLevel < 4)
                {
                    diveZone1.diveLevel ++;
                    diveZone2.diveLevel ++;
                }
                break;
            case 2:
                playerSpeed.baseMovementSpeed += playerSpeed.speedIncrease;
                break;
            case 3:
                worker.maxBaloSize++;
                break;
        }
    }
}
