using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UCExtension.GUI;
using API.Ads;

public class TreasureRewardGUI : BaseGUI
{
    [SerializeField] Button claimButton;
    [SerializeField] Text moneyText;
    [SerializeField] Animator shellRewardAnim;
    [SerializeField] int money;

    void Start()
    {
        claimButton.onClick.AddListener(Claim);
        money = int.Parse(moneyText.text);
    }

    void Claim()
    {
        if (AdManager.Ins)
        {
            AdManager.Ins.ShowRewardedVideo("TreasureReward", (finished) =>
            {
                if (finished)
                {
                    OnClaim();
                }
            });
        }
        else
        {
            OnClaim();
        }
    }

    void OnClaim()
    {
        DataManager.Ins.Coin += money;
        HomeGUI.Ins.moneyText.text = DataManager.Ins.Coin.ToString();
        base.Close();
    }
    void OnEnable()
    {
        ReturnMoneyInCase();
        moneyText.text = money.ToString();
        shellRewardAnim.SetTrigger("rewardpop");
    }

    void ReturnMoneyInCase()
    {
        var usedCoin = DataManager.Ins.UsedCoin;
        if (usedCoin < 100)
        {
            money = 50;
        }
        else
        {
            money = 50 + ((usedCoin - 100) / 300) * 45;
        }
    }
}
