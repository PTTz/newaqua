using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayCaller 
{
    float timeDelay;

    float lastTimeCall;

    public DelayCaller(float timeDelay)
    {
        this.timeDelay = timeDelay;
    }

    public bool Call()
    {
        if (Time.time > lastTimeCall + timeDelay)
        {
            ResetLastTime();
            return true;
        }
        return false;
    }

    public void ResetLastTime()
    {
        lastTimeCall = Time.time;
    }
}
