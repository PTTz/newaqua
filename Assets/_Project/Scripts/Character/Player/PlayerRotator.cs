using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotator : MonoBehaviour
{
    [SerializeField] Transform horizontalRotator;

    [SerializeField] Transform verticalRotator;

    [SerializeField] float rotationSpeed;

    [SerializeField] float changeSpeed;

    [SerializeField] float thresold;

    Vector3 targetAngle = Vector3.zero;

    float minXAngle = -40f;

    float maxXAngle = 40;

    public void ResetAngle()
    {
        targetAngle = new Vector3(0, 0, 0);
    }
    // Update is called once per frame
    void Update()
    {
        var targetRotation = Quaternion.Euler(new Vector3(targetAngle.x, 0, 0));
        if (Quaternion.Angle(verticalRotator.rotation, targetRotation) < thresold) return;
        verticalRotator.localRotation = Quaternion.LerpUnclamped(verticalRotator.localRotation, targetRotation, rotationSpeed * Time.timeScale);
        //
        targetRotation = Quaternion.Euler(new Vector3(0, targetAngle.y, 0));
        if (Quaternion.Angle(horizontalRotator.rotation, targetRotation) < thresold) return;
        horizontalRotator.rotation = Quaternion.LerpUnclamped(horizontalRotator.rotation, targetRotation, rotationSpeed * Time.timeScale);
    }

    public void OnChangeRotationInput(Vector2 deltaPosition)
    {
        targetAngle.x += -changeSpeed * deltaPosition.y;
        targetAngle.x = Mathf.Clamp(targetAngle.x, minXAngle, maxXAngle);
        targetAngle.y += changeSpeed * deltaPosition.x;
    }

}
