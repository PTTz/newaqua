using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish3DShow : MonoBehaviour
{
    [SerializeField] List<GameObject> fishmodel;
    int i = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Rotate(Vector3.up);
    }
    public void OnShowing()
    {
        var curlevel = DataManager.Ins.WorldLevel;
        var modelPrefab = DataManager.Ins.GetFish3D(curlevel-1);
        fishmodel.Add(Instantiate(modelPrefab, transform));
    }

    public void NotShowing()
    {
        //Debug.Log("nani");
        Destroy(fishmodel[0]);
        fishmodel.Remove(fishmodel[0]);
    }
}
