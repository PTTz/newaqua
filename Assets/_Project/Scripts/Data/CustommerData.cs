using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "CustommerDataList", menuName = "Data/CustommerDataList")]
public class CustommerData : ScriptableObject
{
    public List<UsedCoinLevel> NumberCustommerDataList;
}

[Serializable]
public class UsedCoinLevel
{
    public int NumberOfCustommer;
    public int UsedCoin;
}
