using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterCollider : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if(other.tag.Equals(TagName.Worker))
        {
            var charController = other.GetComponent<WorkerBot>();
            charController.isSwiming = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals(TagName.Worker))
        {
            var charController = other.GetComponent<WorkerBot>();
            charController.isSwiming = false;
        }
    }
}
