using API.Ads;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceUpgrade : MonoBehaviour
{
    [SerializeField] Image upgradeIcon;
    [SerializeField] Text upgradeName;
    [SerializeField] Text upgradeLevel;
    [SerializeField] Text moneyText;
    [SerializeField] Text claimText;
    [SerializeField] Text maxText;

    [SerializeField] int level;
    [SerializeField] int money;
    [SerializeField] int claim;

    [SerializeField] ListUpgrade listUpgrades;
    public int upgradeIndex;

    [SerializeField] Button buyButton;
    [SerializeField] Button claimButton;
    void Start()
    {
        upgradeIcon.sprite = listUpgrades.upgradeData[upgradeIndex].icon;
        upgradeIcon.SetNativeSize();
        upgradeName.text = listUpgrades.upgradeData[upgradeIndex].upgradeName;

        level = DataManager.Ins.GetUpgradeLevel(upgradeIndex);
        claim = DataManager.Ins.ClaimGet(upgradeIndex);
        if(level == 0)
        level++;
        CheckIfMax();

        upgradeLevel.text = "Lv" + level.ToString();
        LoadMoneyClaimText();

        buyButton.onClick.AddListener(OnClickBuy);
        claimButton.onClick.AddListener(OnClickClaim);
    }

    void OnClickBuy()
    {
        if (DataManager.Ins.Coin >= money)
        {
            //tru tien
            DataManager.Ins.Coin -= money;
            claim = 0;
            DataManager.Ins.SetClaimUpgrade(upgradeIndex, claim);
            Upgrade();
        }
    }

    void OnClickClaim()
    {
        if (AdManager.Ins)
        {
            AdManager.Ins.ShowRewardedVideo("adsUpgrade", (finished) =>
            {
                if (finished)
                {
                    OnClaim();
                }
            });
        }
        else
        {
            OnClaim();
        }
    }
    void OnClaim()
    {
        claim++;
        if (claim == listUpgrades.upgradeData[upgradeIndex].costs[level].AdsCost)
        {
            Upgrade();
            claim = 0;
            DataManager.Ins.SetClaimUpgrade(upgradeIndex, claim);
        }
        else
        {
            DataManager.Ins.SetClaimUpgrade(upgradeIndex, claim);
        }
        claimText.text =
                "Claim "
                + claim.ToString()
                + "/"
                + listUpgrades.upgradeData[upgradeIndex].costs[level].AdsCost.ToString();
    }
    void Upgrade()
    {
        level++;
        DataManager.Ins.SetUpgradeLevel(upgradeIndex, level);

        upgradeLevel.text = "Lv" + level.ToString();
        LoadMoneyClaimText();
        CheckIfMax();
    }
    void CheckIfMax()
    {
        if (level == listUpgrades.upgradeData[upgradeIndex].costs.Count)
        {
            upgradeLevel.text = "MaxLv" + (level - 1).ToString();
            buyButton.gameObject.SetActive(false);
            claimButton.gameObject.SetActive(false);
            maxText.gameObject.SetActive(true);
        }
    }
    void LoadMoneyClaimText()
    {
        if(level < listUpgrades.upgradeData[upgradeIndex].costs.Count)
        {
            money = listUpgrades.upgradeData[upgradeIndex].costs[level].MoneyCost;
            moneyText.text = money.ToString();

            claimText.text = 
                "Claim " 
                + claim.ToString()
                +"/"
                + listUpgrades.upgradeData[upgradeIndex].costs[level].AdsCost.ToString();
        }
    }
}
