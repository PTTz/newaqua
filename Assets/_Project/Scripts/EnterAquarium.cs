using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterAquarium : PlayerInteractionZone
{
    public override void OnPlayerEnter(Player player)
    {
        base.OnPlayerEnter(player);
        GameController.Ins.TutorialUnlock(4);
    }
}
