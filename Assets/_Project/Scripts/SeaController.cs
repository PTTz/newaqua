using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UCExtension.Common;
public class SeaController : MonoBehaviour
{
    [SerializeField] RecylableObject fishLevel1;

    // Start is called before the first frame update
    void Start()
    {
        //SpawnFish();
    }

    void SpawnFish()
    {
        for (int i = 0; i < 30; i++)
        {
            spawnFishLevelOne();
        }
    }

    public void spawnFishLevelOne()
    {
        var fishLevelOne = PoolObjects.Ins.SpawnObject(fishLevel1 , gameObject.transform);

        fishLevelOne.transform.localPosition = new Vector3(Random.Range(-25f, 60f) * 0.64f, -0.5f , Random.Range(-8f, 50f));
    }
}
