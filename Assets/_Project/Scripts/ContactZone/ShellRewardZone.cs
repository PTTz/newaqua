using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UCExtension.GUI;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class ShellRewardZone : MonoBehaviour
{
    [SerializeField] int id;
    [SerializeField] Image LoadingFill;
    [SerializeField] float originTimer;
    [SerializeField] float timer;
    [SerializeField] bool isTriggering = false;
    [SerializeField] Player player;
    private void Start()
    {
        LoadingFill.fillAmount = 0f;
        timer = originTimer;
    }
    private void Update()
    {
        if (isTriggering)
        {
            timer -= Time.deltaTime;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isTriggering = true;
            StartCoroutine(WaitFill());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(WaitShutDownFill());
            isTriggering = false;
            timer = originTimer;
            GUIController.Ins.HideGUI("ShellRewardGUI");
        }
    }
    private IEnumerator WaitShutDownFill()
    {
        yield return new WaitForSeconds(0.5f);
        LoadingFill.fillAmount = 0f;
    }
    private IEnumerator WaitFill()
    {
        //print("This bitch still going");
        DOVirtual.Float(LoadingFill.fillAmount, 1f, originTimer, v => 
        {
            LoadingFill.fillAmount = isTriggering ? v : 0f;
        }).SetEase(Ease.Linear);
        yield return new WaitUntil(() => (originTimer - timer) > originTimer);
        Debug.Log("CALL DEEP DIVE");
        player.DeepDive();
        yield return new WaitForSeconds(2f);
        GUIController.Ins.ShowGUI("ShellRewardGUI");
    }
}
