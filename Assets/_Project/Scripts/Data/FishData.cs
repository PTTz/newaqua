using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FishData", menuName = "Data/FishData")]
public class FishData : ScriptableObject
{
    public FishType FishType_;
    public int FishHP;
    public int FishLevel;
    public int NormalSpeed;
    public int EscapingSpeed;
    public int MoneyValue;
    public string fishName;
    public Sprite fishIcon;
    public GameObject fish3D;
}

public enum FishType
{
    ClownFish,
    PetFish,
    BlueAngel, 
    Seahouse, 
    Octopus, 
}