using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using DG.Tweening;

public class BuySpaceZone : BuyZone
{
    public Transform WallEndBot;
    public Transform Tank5;
    public override void UnLock()
    {
        base.UnLock();
        WallEndBot.DOMoveZ(transform.position.z - 20, 0.5f).OnComplete(()=>
        {
            Tank5.transform.DOMoveY(0, 0.5f);
        });

    }
    public override void InstantUnlock()
    {
        base.InstantUnlock();
        WallEndBot.DOMoveZ(transform.position.z - 20, 0f).OnComplete(() =>
        {
            Tank5.transform.DOMoveY(0, 0.5f);
        });
    }
}
