using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]
public class NavMeshAgentController : MonoBehaviour
{
    protected NavMeshAgent navMeshAgent;


    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }
    public void Active(bool isActive)
    {
        navMeshAgent.enabled = isActive;
    }
    public void StopMove()
    {
        navMeshAgent.SetDestination(transform.position);
    }

    private void Start()
    {
        navMeshAgent.Warp(transform.position);
    }
    public void KnockBack(Transform agent, float range, UnityAction finish = null)
    {
        var direction = transform.position - agent.position;
        Vector3 destination = GetKnockBackPosition(direction, range);
        navMeshAgent.SetDestination(destination);
        float prevSpeed = navMeshAgent.speed;
        float prevAngularSpeed = navMeshAgent.angularSpeed;
        navMeshAgent.speed = 1000f;
        navMeshAgent.angularSpeed = 0;
        DOVirtual.DelayedCall(2, () =>
        {
            navMeshAgent.speed = prevSpeed;
            Debug.Log("Prev speed: " + prevSpeed);
            navMeshAgent.angularSpeed = prevAngularSpeed;
            finish?.Invoke();
        });
    }

    Vector3 GetKnockBackPosition(Vector3 direction, float range)
    {
        Vector3 infor = new Vector3();
        Vector3 offset = direction.normalized * range;
        infor = transform.position + offset;
        NavMeshHit hit;
        bool isHitted = NavMesh.Raycast(transform.position, infor, out hit, NavMesh.AllAreas);
        if (isHitted)
        {
            infor = hit.position + (transform.position - hit.position).normalized;
        }
        return infor;
    }
}
