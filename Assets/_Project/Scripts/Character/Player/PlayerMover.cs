using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMover : NavMeshAgentController
{
    [SerializeField] Transform mainBody;

    public float baseMovementSpeed = 10f;

    public float speedIncrease = 5 / 10f;

    [SerializeField] float rotationSpeed;

    Quaternion targetRotation = Quaternion.identity;

    public void OnChangeMovementInput(Vector2 movementInput)
    {
        Vector3 force = new Vector3(movementInput.x, 0f, movementInput.y);
        Vector3 lookPos = mainBody.position + force;
        targetRotation = Quaternion.LookRotation(force);
        navMeshAgent.Move(force * baseMovementSpeed * Time.deltaTime);
    }

    //public float GetSpeedPlayer()
    //{
    //    float speed = ;
    //    return speed;
    //}
    private void FixedUpdate()
    {
        mainBody.rotation = Quaternion.Lerp(mainBody.rotation, targetRotation, rotationSpeed * Time.fixedDeltaTime);
        if (Quaternion.Angle(mainBody.rotation, targetRotation) > 0.5f)
        {
        }
        //trans.eulerAngles = new Vector3(0, Mathf.Lerp(trans.eulerAngles.y, targetAngle, rotationSpeed * Time.fixedDeltaTime), 0);
    }
    //public void ResetMovementSpeed()
    //{
    //    currentMovementSpeed = DataManager.Ins.MoveSpeedLevel * speedIncrease + baseMovementSpeed;
    //}
}
