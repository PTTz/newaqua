using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ListFish", menuName = "Create List Fish")]
public class ListFish : ScriptableObject
{
    public FishData[] fishData;
}
