using System.Collections;
using System.Collections.Generic;
using UCExtension.Common;
using UnityEngine;

public class MoneyZone : MonoBehaviour
{
    //public bool CanPopObject => products.Count > 0;
    [SerializeField] float defaultPlusHeight;
    [SerializeField] List<Transform> pos;
    [SerializeField] RecylableObject MoneyObj;

    public Stack<MovingObject> carringMoneys = new Stack<MovingObject>();

    float defaultY;
    bool firstLoaded = false;
    private void Start()
    {
        defaultY = pos[0].position.y;
        FirstLoad();
    }
    void FirstLoad()
    {
        for (int i = 0; i < DataManager.Ins.CurrentMoneyOnTable; i++)
        {
            OnPushMoney(transform);
        }
        firstLoaded = true;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals(TagName.Player))
        {
            OnPlayerStay(other);
        }
    }
    void OnPlayerStay(Collider other)
    {
        if (carringMoneys.Count == 0) return;
        var pickedObj = carringMoneys.Pop();
        DataManager.Ins.CurrentMoneyOnTable = carringMoneys.Count;
        var money3D = pickedObj.GetComponent<Money3D>();
        money3D.Jump(other.transform.position, 0.5f, finish: () =>
        {
            DataManager.Ins.Coin += money3D.value;
            money3D.Recyle();
        });
        GameController.Ins.TutorialUnlock(7);
    }
    public void OnPushMoney(Transform from)
    {
        var newMoneyObj = PoolObjects.Ins.SpawnObject(MoneyObj, from);
        var newMovingObj = newMoneyObj.GetComponent<MovingObject>();
        newMoneyObj.transform.parent = GameController.Ins.MoneyZone.transform;
        float x = GetMoneyPosX();
        float y = GetMoneyPosY();
        float z = GetMoneyPosZ();
        Vector3 moneyPos = new Vector3(x, y, z);
        newMovingObj.Jump(moneyPos, 0.5f);
        GameController.Ins.MoneyZone.carringMoneys.Push(newMovingObj);
        if (!firstLoaded) return;
        DataManager.Ins.CurrentMoneyOnTable = carringMoneys.Count;
    }
    float GetMoneyPosX()
    {
        int indexOnFlat = carringMoneys.Count % 9;
        return pos[indexOnFlat].position.x;
    }
    float GetMoneyPosY()
    {
        int floor = carringMoneys.Count / 9;
        return floor * defaultPlusHeight + defaultY;
    }
    float GetMoneyPosZ()
    {
        int indexOnFlat = carringMoneys.Count % 9;
        return pos[indexOnFlat].position.z;
    }

}
