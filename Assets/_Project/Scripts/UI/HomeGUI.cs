﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UCExtension.GUI;
using UCExtension.Common;
public class HomeGUI : Singleton<HomeGUI>
{
    [SerializeField] Button settingButton;

    [SerializeField] Text levelText;

    [SerializeField] Player player;

    [SerializeField] Text sizeText;

    [SerializeField] Text[] fishAmountText;

    [SerializeField] int[] fishAmount;

    [SerializeField] GameObject[] fishHolder;

    public Text moneyText;
    // Start is called before the first frame update
    void Start()
    {
        settingButton.onClick.AddListener(OnSettingButtonClick);
        //CountOnStart();
    }
    // Update is called once per frame
    void Update()
    {
        moneyText.text = DataManager.Ins.Coin.ToString();
        levelText.text = DataManager.Ins.WorldLevel.ToString();
        sizeText.text = player.carringFishList.Count.ToString() + " / " + player.MaxBaloSize.ToString();
    }

    public void OnCatched(int index)
    {
        fishAmount[index] += 1;
        fishAmountText[index].text = fishAmount[index].ToString();
        if (!fishHolder[index].activeInHierarchy)
        {
            fishHolder[index].SetActive(true);
        }
    }

    public void OnRemoveFish(int index)
    {
        fishAmount[index] -= 1;
        fishAmountText[index].text = fishAmount[index].ToString();
        if (fishAmount[index] == 0) 
        {
            fishHolder[index].SetActive(false);
        }
    }

    //balo bị reset mỗi khi vào game nên đoạn này không cần lắm
    /*void CountOnStart()
    {
        foreach(var carriedFish in player.carringFishList)
        {
            fishAmount[carriedFish.Fish.FishLevel] += 1;
        }

        for(int i=0; i < fishAmountText.Length; i++)
        {
            fishAmountText[i].text = fishAmount[i].ToString();
        }
    }*/

    void OnSettingButtonClick()
    {
        GUIController.Ins.ShowGUI("SettingGUI");
    }
}
