using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockByWorldLevel : MonoBehaviour
{
    public int LockLevel;
    // Start is called before the first frame update
    void Start()
    {
        DataManager.Ins.OnWorldLevelChange += OnWorldLevelChange;
        OnWorldLevelChange(DataManager.Ins.WorldLevel);
    }
    void OnWorldLevelChange(int worldLevel)
    {

        if (LockLevel <= worldLevel)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
