using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeTable : MonoBehaviour
{
    [SerializeField] UpgraderBotAnim UpgraderAnim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckDistant(gameObject.transform, GameController.Ins.MainPlayer.transform, 10))
        {
            UpgraderAnim.Wave();
        }
        else
        {
            UpgraderAnim.Wait();
        }
    }

    public bool CheckDistant(Transform from, Transform to, float isSmallerThisFloat)
    {
        float distant = 0;
        distant = Vector3.Distance(from.position, to.position);
        if (distant > isSmallerThisFloat)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
