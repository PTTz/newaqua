using UnityEngine;

public class WorkerGoFishingState : BaseState<WorkerBot>
{
    public override void Enter()
    {
        base.Enter();
        TargetNewFish();
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
        owner.AnimationCheck();
        if (!owner.fishTargeted)
        {
            TargetNewFish();
        }
        else
        {
            owner.botMover.SetDestination(owner.fishTargeted.gameObject.transform.position);
        }
        if (owner.CheckDistant(owner.transform, owner.fishTargeted.transform, 2))
        {
            owner.isNearTarget = true;
        }
        else
        {
            owner.isNearTarget = false;
        }
    }
    void TargetNewFish()
    {
        if(GameController.Ins.fishAbleToCatchList.Count > 0)
        {
            owner.fishTargeted = GetNeastAbleFish();
            if (owner.fishTargeted)
            {
                owner.fishTargeted.OnCatched += CatchedFish;
            }
        }
    }
    FishController GetNeastAbleFish()
    {
        float smallestDistant = 9999;
        var fish = GameController.Ins.fishAbleToCatchList[0];
        foreach (var newFish in GameController.Ins.fishAbleToCatchList)
        {
            if(owner.CheckDistant(owner.transform, newFish.transform, smallestDistant))
            {
                fish = newFish;
                smallestDistant = Vector3.Distance(owner.transform.position, newFish.transform.position);
            }
        }
        return fish;
    }
    void CatchedFish(FishController fish, Character hunter)
    {
        if(owner != hunter)
        {
            TargetNewFish();
        }
        else
        {
            owner.CatchedNewFish(fish, hunter);
        }
        if (owner.carringFishList.Count >= owner.maxBaloSize)
        {
            owner.stateMachine.ChangeState<WorkerBringFishToTankState>();
        }
        //Debug.Log("<color=red> Catch fish, now target new fish</color>");
        TargetNewFish();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
