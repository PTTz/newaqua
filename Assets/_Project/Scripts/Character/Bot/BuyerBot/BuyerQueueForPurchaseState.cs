
using UnityEngine;

public class BuyerQueueForPurchaseState : BotBaseState
{
    public override void EnterState(BuyerBot buyer)
    {

    }
    public override void UpdateState(BuyerBot buyer)
    {
        CheckToGoToNextPosInQueue(buyer);
    }
    void CheckToGoToNextPosInQueue(BuyerBot buyer)
    {
        if(buyer.CurrentPosInQueue != 0)
        {
            var currentPos = GameController.Ins.CashierTable.CurrentPosInQueue(buyer.CurrentPosInQueue);
            var nextPos = GameController.Ins.CashierTable.NextPosInQueue(buyer.CurrentPosInQueue);
            if (!nextPos.Buyer)
            {
                buyer.CurrentPosInQueue--;
                currentPos.Buyer = null;
                nextPos.Buyer = buyer;
                buyer.TargetPos = nextPos.Pos;
                buyer.botMover.SetDestination(buyer.TargetPos.position);

            }
        }
        if(IsNearTarget(buyer.transform,buyer.TargetPos))
        {
            Debug.Log("IDLE");
            buyer.IdleWithFishInHead();
        }
        else
        {
            Debug.Log("RUN");
            buyer.RunWithFishInHead();
        }
    }
    bool IsNearTarget(Transform from, Transform to)
    {
        var distant = Vector3.Distance(from.position, to.position);
        if (distant < 0.02f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public override void OnTriggerEnter(BuyerBot buyer, Collider other)
    {

    }
    public override void ExitState(BuyerBot buyer)
    {

        buyer.GiveMoney(buyer.transform);
    }
}
