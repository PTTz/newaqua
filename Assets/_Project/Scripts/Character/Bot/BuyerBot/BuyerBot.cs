//using API.UI;
using System.Collections;
using System.Collections.Generic;
using UCExtension.Common;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class BuyerBot : Character
{
    public BotBaseState currentState { get; private set; }
    public BuyerWanderState wanderState = new BuyerWanderState();
    public BuyerChoosingFishState choosingFishState = new BuyerChoosingFishState();
    public BuyerGotoPurchaseState gotoPurchaseState = new BuyerGotoPurchaseState();
    public BuyerQueueForPurchaseState queueForPurchaseState = new BuyerQueueForPurchaseState();
    public BuyerWaitForPurchaseState waitForPurchaseState = new BuyerWaitForPurchaseState();
    public BuyerDonePurchaseState donePurchaseState = new BuyerDonePurchaseState();
    public BuyerIdleState idleState = new BuyerIdleState();

    public Transform TargetPos;
    public FishTank TankTargeting;
    public FishController carringFish;
    public Canvas Canvas;

    public BotMover botMover;

    public Transform FishPos;

    public int CurrentPosInQueue;

    public Image FishImg;

    public Image HappyFaceImg;

    public Image DotImg;

    public NavMeshAgent myAgent;

    public float TimeToPickFish;

    public GameObject miniTank;

    public RecylableObject MoneyObj;
    void Start()
    {
        currentState = wanderState;
        currentState.EnterState(this);
        miniTank.SetActive(false);
    }
    void Update()
    {
        currentState.UpdateState(this);
    }
    private void OnTriggerEnter(Collider other)
    {
        currentState.OnTriggerEnter(this, other);
    }
    public void HappyFace()
    {
        FishImg.gameObject.SetActive(false);
        HappyFaceImg.gameObject.SetActive(true);
        DotImg.gameObject.SetActive(false);
    }
    public void Walk(float speed)
    {
        animationController.WalkingSpeed(speed);
    }
    public void RunWithFishInHead()
    {
        animationController.HoldType(1);
    }
    public void IdleWithFishInHead()
    {
        animationController.HoldType(0);
    }
    public void SelfDestruction()
    {
        Destroy(gameObject);
    }
    public void INeedThisFish(FishData fish)
    {
        Canvas.gameObject.SetActive(true);
        FishImg.gameObject.SetActive(true);
        HappyFaceImg.gameObject.SetActive(false);
        DotImg.gameObject.SetActive(false);
        FishImg.sprite = fish.fishIcon;
    }
    public void WhereIsMyFish()
    {
        DotImg.gameObject.SetActive(true);
        FishImg.gameObject.SetActive(false);
        HappyFaceImg.gameObject.SetActive(false);
    }
    public void SwitchState(BotBaseState state)
    {
        currentState.ExitState(this);
        currentState = state;
        currentState.EnterState(this);
    }
    public void GiveMoney(Transform from)
    {
        int prefabNum = carringFish.Fish.MoneyValue / MoneyObj.GetComponent<Money3D>().value;
        for (int i = 0; i < prefabNum; i++)
        {
            SpawnMoney(from);
        }
    }
    void SpawnMoney(Transform from)
    {
        GameController.Ins.MoneyZone.OnPushMoney(from);
    }
    public bool IsNearTarget()
    {
        float distant = Vector3.Distance(transform.position, TargetPos.position);
        if (distant < 3.5f) return true;
        else return false;
    }
    public override void Recyle()
    {
        GameController.Ins.customerBots.Remove(this);
        currentState = wanderState;
        TargetPos = null;
        Canvas.gameObject.SetActive(false);
        TankTargeting = null;
        carringFish.Recyle();
        carringFish = null;
        miniTank.SetActive(false);
        base.Recyle();
    }
}
