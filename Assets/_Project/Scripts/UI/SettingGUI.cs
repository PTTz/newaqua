using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UCExtension.GUI;

public class SettingGUI : BaseGUI
{
    [SerializeField] ButtonSetting joystickButton;

    [SerializeField] ButtonSetting vibrationButton;

    [SerializeField] ButtonSetting musicButton;

    [SerializeField] ButtonSetting soundButton;

    [SerializeField] Button closeButton;

    void Start()
    {
        joystickButton.OnClickButton(DataManager.Ins.IsJoystick);
        vibrationButton.OnClickButton(DataManager.Ins.IsOnVibration);
        musicButton.OnClickButton(DataManager.Ins.IsOnMusic);
        soundButton.OnClickButton(DataManager.Ins.IsOnSound);

        closeButton.onClick.AddListener(Close);
    }

    public void OnClickJoystickButton()
    {
        DataManager.Ins.IsJoystick = !joystickButton.buttonStatus;

        joystickButton.OnClickButton(DataManager.Ins.IsJoystick);
    }

    public void OnClickVibrationButton()
    {
        DataManager.Ins.IsOnVibration = !vibrationButton.buttonStatus;

        vibrationButton.OnClickButton(DataManager.Ins.IsOnVibration);
    }

    public void OnClickMusicButton()
    {
        DataManager.Ins.IsOnMusic = !musicButton.buttonStatus;

        musicButton.OnClickButton(DataManager.Ins.IsOnMusic);
    }

    public void OnClickSoundButton()
    {
        DataManager.Ins.IsOnSound = !soundButton.buttonStatus;

        soundButton.OnClickButton(DataManager.Ins.IsOnSound);
    }


    public void Close()
    {
        base.Close();
    }
}
