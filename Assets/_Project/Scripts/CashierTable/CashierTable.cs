using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CashierTable : MonoBehaviour
{
    [SerializeField] bool haveCashier;
    [SerializeField] List<QueringPos> queringPosList;
    public CashierZone cashierZone;
    public Transform CashierStayPos;

    private void Awake()
    {
        cashierZone.OnEnter += CashierEnter;
        cashierZone.OnExit += CashierExit;
    }
    private void Update()
    {
        if (haveCashier)
        {
            var buyer = queringPosList[0].Buyer;
            if (buyer && IsNearTarget(buyer.transform, queringPosList[0].Pos))
            {
                ProgressPurchase(queringPosList[0].Buyer);
            }
        }
    }
    bool IsNearTarget(Transform from, Transform to)
    {
        var distant = Vector3.Distance(from.position, to.position);
        if (distant < 0.5f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void ProgressPurchase(BuyerBot buyer)
    {
        if (buyer.currentState != buyer.donePurchaseState)
        {
            queringPosList[0].Buyer = null;
        }
        buyer.SwitchState(buyer.donePurchaseState);
        GameController.Ins.TutorialUnlock(6);
    }
    public QueringPos TargetNearestAblePos(BuyerBot buyer)
    {
        for (int i = 0; i < queringPosList.Count; i++)
        {
            if (!queringPosList[i].Buyer)
            {
                NewBuyerQueue(buyer, i);
                return queringPosList[i];
            }
        }
        return null;
    }
    void NewBuyerQueue(BuyerBot buyer, int posIndex)
    {
        queringPosList[posIndex].Buyer = buyer;
        buyer.CurrentPosInQueue = posIndex;
    }
    public QueringPos CurrentPosInQueue(int posIndex)
    {
        return queringPosList[posIndex];
    }
    public QueringPos NextPosInQueue(int posIndex)
    {
        return queringPosList[posIndex - 1];
    }
    public void BuyerCompletePurchase(BuyerBot buyer)
    {

    }
    void CashierEnter(Collider other)
    {
        if (other.tag.Equals(TagName.Player) || other.tag.Equals(TagName.Cashier))
        {
            haveCashier = true;
        }
    }
    void CashierExit(Collider other)
    {
        if (other.tag.Equals(TagName.Player) || other.tag.Equals(TagName.Cashier))
        {
            haveCashier = false;
        }
    }
}

[Serializable]
public class QueringPos
{
    public Transform Pos;
    public BuyerBot Buyer;
}